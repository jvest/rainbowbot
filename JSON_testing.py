# Rainbowbot Copyright (C) 2012 Joe Vest
# -------------------------------------------------
# Author:  Joe Vest
# Contact: mrjoevest at gmail.com
#   License details included with this application.
#   Referer to LICENSE.md for license details
# -------------------------------------------------
#
# JSON_testing.py

# JSON Request Testing
import json, requests, ConfigParser
import string, random

# Read and Set Configuration from Config File
Config = ConfigParser.ConfigParser()
Config.read('./app.conf')


def ConfigSectionMap(section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

client_username = ConfigSectionMap('CLIENT')['client_username']
client_password = ConfigSectionMap('CLIENT')['client_password']
server_ip     = ConfigSectionMap('SERVER')['server_ip']
apiURL          = 'https://' + server_ip + '/ReceiveJSON'
httpheaders     = {"Content-type": "application/json"}

# Random ID Generator
def id_generator(size=16, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

# Generate Random agentid
agentid = id_generator()

# Alive Test JSON
# Jobtype 1 with jobpayload of agentid
payload = {'jobtype': 1, 'jobpayload': {'agentid': agentid}}
json_payload = json.dumps(payload)
r = requests.post(apiURL,data=json_payload, headers=httpheaders,verify=False,auth=(client_username,client_password))
print '=' * 25
print 'Alive Test Results'
print r 
print r.text

# Register Test JSON
# Jobtype 2 with jobpayload of agentid

osinfo   = 'OS_' + agentid
hostname = 'HOST_' + agentid

payload = {'jobtype': 2, 'jobpayload': {'agentid': agentid, 'osinfo':osinfo, 'hostname': hostname}}
json_payload = json.dumps(payload)
r = requests.post(apiURL,data=json_payload, headers=httpheaders,verify=False,auth=(client_username,client_password))
print '=' * 25
print 'Register Test Results'
print r 
print r.text











