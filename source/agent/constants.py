# Rainbowbot Copyright (C) 2012 Joe Vest
# -------------------------------------------------
# Author:  Joe Vest
# Contact: mrjoevest at gmail.com
#   License details included with this application.
#   Referer to LICENSE.md for license details
# -------------------------------------------------
# 
# contants.py

httpcclist = "192.168.1.146:443,127.0.0.1:443"
useragentstring = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 1.1.4322; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648)"
initialwaittime = "5"
polltime = "5"
polldrift = "5"
allowedtimes = "0001,2359"
killdate = "20140101"
protocol = "HTTPS"
debug = "True"
client_username = "username"
client_password = "password"
randomstring = "H30YQDQL6HG"
