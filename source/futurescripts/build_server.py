import ConfigParser, fileinput, re, subprocess, os

# Read and Set Configuration from Config File
Config = ConfigParser.ConfigParser()
Config.read('./CONFIGURATION.ini')

def ConfigSectionMap(section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

# Python Parameters
PYTHON_WIN = ConfigSectionMap('PYTHON')['python_win']
PYTHON_NX = ConfigSectionMap('PYTHON')['python_nx']
PYINSTALLER = ConfigSectionMap('PYTHON')['pyinstaller']
OUTPUT = ConfigSectionMap('PYTHON')['output']
SERVERSOURCE = ConfigSectionMap('PYTHON')['serversource']   

if os.name == 'nt':
    PYTHON = PYTHON_WIN
else: 
    PYTHON = PYTHON_NX

# Pyinstaller Commandline
command = PYTHON + " " + PYINSTALLER + " --onefile" + " --out=" + OUTPUT + " --console " + SERVERSOURCE



# Build server.py

subprocess.call(command, shell=True)


