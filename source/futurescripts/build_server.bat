REM@echo off

set python=c:\Python27\python.exe
set pyinstaller=c:\Python27\pyinstaller-2.0\pyinstaller.py
set output=.\build
set bin=%output%\dist\server.exe
set input=.\source\server\server.py

if exist %bin% del /q %bin%


echo ************************************
echo Building EXE ...
%python% %pyinstaller% --onefile --out=%output% --console %input% 2>build_log.txt
echo Complete


pause
