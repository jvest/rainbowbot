# Rainbowbot Copyright (C) 2012 Joe Vest
# -------------------------------------------------
# Author:  Joe Vest
# Contact: mrjoevest at gmail.com
#   License details included with this application.
#   Referer to LICENSE.md for license details
# -------------------------------------------------
# 
# display.py

# all the imports
import sqlite3
import ConfigParser

# Read and Set Configuration from Config File
Config = ConfigParser.ConfigParser()
Config.read('../../app.conf')


def ConfigSectionMap(section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

DATABASE = ConfigSectionMap('COMMON')['database']

#SECRET_KEY = 'development key'
#USERNAME = 'admin'
#PASSWORD = 'default'


# Connect to DB
def connect_db():
    return sqlite3.connect(DATABASE)
    
con = connect_db()

print '*' * 80
    
with con:
    print "Parameters"
    sql = 'select * from parameters'
    cur = con.execute(sql)
    entries = cur.fetchall()
    for item in entries:
        print item

print '*' * 80
        
with con:
    print "Agents"
    sql = 'select * from agents'
    cur = con.execute(sql)
    entries = cur.fetchall()
    for item in entries:
        print item

print '*' * 80        
  
with con:
    print "Jobs"
    sql = 'select * from jobs'
    cur = con.execute(sql)
    entries = cur.fetchall()
    for item in entries:
        print item  
        
print '*' * 80
    
   
    
if con:
    con.close()
    
    
    
string = 'add command dir c:\\'
args = string.split(" ")

print args[0] 
print args[1]
print args[2:]
print ' '.join(args[2:])


