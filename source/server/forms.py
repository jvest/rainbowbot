from wtforms import Form, BooleanField, TextField, PasswordField, SelectField, validators

class AddJob(Form):

    jobtype = SelectField('Type', choices=[('command', 'command'), ('shellcode', 'shellcode'), ('syringe', 'syringe')])
    command = TextField('command', [validators.Required(),validators.Length(min=1)])
