# Rainbowbot Copyright (C) 2012 Joe Vest
# -------------------------------------------------
# Author:  Joe Vest
# Contact: mrjoevest at gmail.com
#   License details included with this application.
#   Referer to LICENSE.md for license details
# -------------------------------------------------
# 
# createdb.py

# all the imports
import sqlite3, ConfigParser

# Read and Set Configuration from Config File
Config = ConfigParser.ConfigParser()
Config.read('../../app.conf')


def ConfigSectionMap(section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

# COMMON
DATABASE = ConfigSectionMap('COMMON')['database']

# AGENT
httpcclist = ConfigSectionMap('CLIENT')['httpcclist']
useragentstring = ConfigSectionMap('CLIENT')['useragentstring']
initialwaittime = ConfigSectionMap('CLIENT')['initialwaittime']
polltime = ConfigSectionMap('CLIENT')['polltime']
polldrift = ConfigSectionMap('CLIENT')['polldrift']
allowedtimes = ConfigSectionMap('CLIENT')['allowedtimes']
protocol = ConfigSectionMap('CLIENT')['protocol']

try:
    print "[-] Creating Database..."
    # Connect to DB
    def connect_db():
        return sqlite3.connect(DATABASE)

    con = connect_db()    

    with con:
        cur = con.cursor()
        
        cur.execute('DROP TABLE IF EXISTS agents')
        cur.execute('DROP TABLE IF EXISTS jobs')
        cur.execute('DROP TABLE IF EXISTS parameters')
        # AGENTS table
        sql = 'CREATE TABLE agents (agentid TEXT PRIMARY KEY, hostname TEXT, osinfo TEXT, createtime TEXT, lastpolltime TEXT)'
        cur.execute(sql)
        # JOBS table
        sql = 'CREATE TABLE jobs (id integer primary key autoincrement, agentid TEXT, status TEXT, jobtype TEXT, command TEXT, cmdresult TEXT, createtime TEXT, completedtime TEXT, pid TEXT)'
        cur.execute(sql)
        # PARAMETERS table
        sql = 'CREATE TABLE parameters (id integer primary key autoincrement, agentid TEXT, httpcclist TEXT, useragent TEXT, initialwaittime INTEGER, polltime INTEGER, polldrift INTEGER, allowedtimes TEXT, protocol TEXT)'
        cur.execute(sql)
        
        # Insert default parameters
        # DEFAULT PARAMETERS table values
        sql = 'INSERT INTO parameters VALUES (?,?,?,?,?,?,?,?,?)'
        #values = 0,'default','192.168.1.78:443,127.0.0.1:443', 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 1.1.4322; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648)',5,5,2,'0001,2359', 'HTTPS'
        values = 0,'default',httpcclist,useragentstring,initialwaittime,polltime,polldrift,allowedtimes,protocol

        
        cur.execute(sql,values)

    print '[+] Database Creation Successful'
except:
    print '[!] Database Creation Failed'

        
if con:
    con.close()
