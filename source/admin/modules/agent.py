''' Agent Module'''

import cmd, sys

# Non-standard imports
import modules

   
class Agent(cmd.Cmd):
    # Agents Options
  

    def __init__(self, arg):
        cmd.Cmd.__init__(self)
        self.myagentid = arg
        self.prompt = "rb:agents:" + self.myagentid + "> "
    
    def do_back(self, line):
        return True
    
    def do_help(self, arg):
        output =  '\nAGENT HELP   '+ '\n'
        output += '================' + '\n'
        output += "\t Command            \t Description" + "\n"
        output += "\t -------            \t -----------" + "\n"        
        output += "\t help               \t Help menu" + "\n"
        output += "\t back               \t Return to main" + "\n"
        output += "\t exit               \t Exit application" + "\n"
        output += "\t show               \t Show current agent details" + "\n"
        output += "\t jobs               \t Jobs Menu for current agent" + "\n"
        output += "\t update <parameter> \t Update parameter value" + "\n"
        output += "\t   Example:" + "\n"
        output += "\t   update httpcclist 192.168.1.1:443,192.168.1.2:443" + "\n"
        output += "\t   update useragent this is my useragent " + "\n"
        output += "\t   update polltime 30" + "\n"
        output += "\t   update polldrift 10" + "\n"
        output += "\t   update allowedtimes 0500,1600" + "\n"
        output += "" + "\n\n"
        
        print output
        
        
    def do_show(self, arg):
    
        args = arg.split()
        
        if len(args) == 0: # Default
            self.showindividual(self.myagentid)
        else:
            print "Invalid aruguments"
            return
            
    def do_jobs(self, arg):
        '''Utilize the AGENT Module'''
       
        args = arg.split()
        
        if len(args) == 0: # Default
            s = modules.jobs.Jobs(self.myagentid)
            s.cmdloop()   
        else:
            print "Invalid aruguments"
            return

    def do_update(self, arg):
    
        args = arg.split(" ")
        
        if len(args) > 0: # Default
            result = self.update_agent_parameter(self.myagentid,args[0],' '.join(args[1:]))
            print args[0] + ' updated'
            return
        else:
            print "Invalid aruguments"
            return
        
    UPDATETYPES = [ 'httpcclist', 'useragent', 'initialwaittime', 'polltime', 'polldrift', 'allowedtimes', 'protocol' ]
        
    def complete_update(self, text, line, begidx, endidx):
        if not text:
            completions = self.UPDATETYPES[:]
        else:
            completions = [ f
                            for f in self.UPDATETYPES
                            if f.startswith(text)
                            ]
        return completions
            
    def showindividual(self, agentid):
            
        '''show current option values'''

        agentlist = self.get_agent_info(agentid)
        parameterlist = self.get_agent_parameters(agentid)
        #Agent list
        agentinfo = ''
        parameterinfo = ''

        #self.numbered_agentlist[:] = []
        for item in agentlist:
            
            agentinfo += "AgentID:\t"  + item[0] + "\n"
            agentinfo += "Hostname:\t" + item[1] + "\n"
            agentinfo += "Created:\t"  + item[3] + "\n"
            agentinfo += "Last Poll:\t"+ item[4] + "\n"
            agentinfo += "OS Info:\t"  + "\n"
            agentinfo += "\t\t" + item[2].replace(", ","\n\t\t") + "\n"
            
        for parameter in parameterlist:
            
            parameterinfo += "C&C List:\t\t"    + str(parameter[2]) + "\n"
            parameterinfo += "Useragent:\t\t"   + str(parameter[3]) + "\n"
            parameterinfo += "Initial Wait(sec):\t"         + str(parameter[4]) + "\n"
            parameterinfo += "Poll Time(sec):\t\t"   + str(parameter[5]) + "\n"
            parameterinfo += "Poll Drift(sec):\t"  + str(parameter[6]) + "\n"
            parameterinfo += "Allowed Time(hrs):\t"+ str(parameter[7]) + "\n"
            parameterinfo += "Protocol:\t"+ str(parameter[8]) + "\n"

            
        
        #Header
        header = "\n"
        header += "------------------------------------------------------------------------------------------\n"  
        
        #Footer
        footer =  "==========================================================================================\n"  
        
                
        print header + agentinfo + header + parameterinfo + footer
        
    def get_agent_info(self, agentid):
        dbconnection = modules.dbconnect.DBconnect()
        dbconnection.connect_db()
        #agentlist =  dbconnection.get_agents()
        agentlist =  dbconnection.get_agent(agentid)
        dbconnection.disconnect_db()
        return agentlist
    
    def get_agent_parameters(self, agentid):
        dbconnection = modules.dbconnect.DBconnect()
        dbconnection.connect_db()
        #parameterlist =  dbconnection.get_agents()
        parameterlist =  dbconnection.get_parameters(agentid)
        dbconnection.disconnect_db()
        return parameterlist
    
    
    def update_agent_parameter(self, agentid, parameter, parametervalue):
        dbconnection = modules.dbconnect.DBconnect()
        result = dbconnection.update_parameter(agentid, parameter, parametervalue)
        dbconnection.connect_db()
     
    def do_exit(self, arg):
        '''Exit the program'''
        sys.exit(1)
        
    

        

   

    

