''' Defaults Module'''

import cmd, sys

# Non-standard imports
import modules

   
class Defaults(cmd.Cmd):
    # Agents Options
  
    def __init__(self):
        cmd.Cmd.__init__(self)
        
        self.prompt = "rb:defaults> "
    
    def do_back(self, line):
        return True
    
    def do_help(self, arg):
        output =  '\nDEFAULT HELP   '+ '\n'
        output += '================' + '\n'
        output += "\t Command     \t\t\t Description" + "\n"
        output += "\t -------     \t\t\t -----------" + "\n"        
        output += "\t help        \t\t\t Help menu" + "\n"
        output += "\t back        \t\t\t Return to main" + "\n"
        output += "\t exit        \t\t\t Exit application" + "\n"
        output += "\t show        \t\t\t Show details of default agent's parameters" + "\n"
        output += "\t update <index> <value>   \t Update value of specified parameter for default agent" + "\n"
        output += "" + "\n\n"
        
        print output
        
        
    def do_show(self, arg):
    
        args = arg.split()
        
        if len(args) == 0: # Default
            self.showindividual('default')
        else:
            print "Invalid auguments "
            return
        
    def do_update(self, arg):
    
        args = arg.split(" ")
        
        if len(args) > 0: # Default
            result = self.update_agent_parameter('default',args[0],' '.join(args[1:]))
            print args[0] + ' updated'
            return
        else:
            print "Invalid aruguments"
            return
        
    UPDATETYPES = [ 'httpcclist', 'useragent', 'initialwaittime', 'polltime', 'polldrift', 'allowedtimes', 'protocol' ]
        
    def complete_update(self, text, line, begidx, endidx):
        if not text:
            completions = self.UPDATETYPES[:]
        else:
            completions = [ f
                            for f in self.UPDATETYPES
                            if f.startswith(text)
                            ]
        return completions
            
            
    def showindividual(self, agentid):
            
        '''show current option values'''

        parameterlist = self.get_agent_parameters(agentid)
        parameterinfo = ''

        
        for parameter in parameterlist:
            
            parameterinfo += "C&C List:\t\t"    + str(parameter[2]) + "\n"
            parameterinfo += "Useragent:\t\t"   + str(parameter[3]) + "\n"
            parameterinfo += "Initial Wait(sec):\t"         + str(parameter[4]) + "\n"
            parameterinfo += "Poll Time(sec):\t\t"   + str(parameter[5]) + "\n"
            parameterinfo += "Poll Drift(sec):\t"  + str(parameter[6]) + "\n"
            parameterinfo += "Allowed Time(hrs):\t"+ str(parameter[7]) + "\n"

            
        
        #Header
        header = "\n"
        header = "DEFAULT PARAMETERS - VALUES WILL BE ASSIGNED TO NEW AGENTS AT REGISTRATION"
        header += "------------------------------------------------------------------------------------------\n"  
        
        #Footer
        footer =  "==========================================================================================\n"  
        
                
        print header + parameterinfo + footer
        
    def get_agent_parameters(self, agentid):
        dbconnection = modules.dbconnect.DBconnect()
        dbconnection.connect_db()
        #parameterlist =  dbconnection.get_agents()
        parameterlist =  dbconnection.get_parameters(agentid)
        dbconnection.disconnect_db()
        return parameterlist
    
    def update_agent_parameter(self, agentid, parameter, parametervalue):
        dbconnection = modules.dbconnect.DBconnect()
        result = dbconnection.update_parameter(agentid, parameter, parametervalue)
        dbconnection.connect_db()
     
    def do_exit(self, arg):
        '''Exit the program'''
        sys.exit(1)
        
    

        

   

    

