"""Util module"""

import logging, cmd
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

from scapy.all import *

class Util(cmd.Cmd):
    def __init__(self):
        cmd.Cmd.__init__(self)
        #self.prompt = "FT (util)>"
        print ""
        
    def do_back(self, line):
        return True
    
    def do_networks(self):
        networks=[]
        tobin = lambda n: n>0 and tobin(n>>1)+str(n&1) or ''
       
        for net,msk,gw,iface,addr in conf.route.routes:
            if iface != 'lo' and gw == '0.0.0.0':
                ip = ltoa(net)
                mask = tobin(msk).count('1')
                networks.append("%s/%s" % (ip,mask))

        if len(networks) > 0:
            print "Local Networks:", len(networks)
            print "==============="
            for net in networks:
                print ''.join(net)
        else:
            print "No networks identified"
    
    def do_updateoui(self):
       
        OUI = './oui.txt'
       
        if os.path.exists(OUI):
            os.remove(OUI)
    
        print "Fetching OUIs..."
        sys.stdout.flush()
        os.system('wget http://standards.ieee.org/regauth/oui/oui.txt -O %s 2> /dev/null' % OUI)
        print "OUI DB saved to", OUI

    

