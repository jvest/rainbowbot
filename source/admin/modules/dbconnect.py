''' DBConnect Module'''
import sqlite3, datetime, ConfigParser


class DBconnect():

    def __init__(self):
        #cmd.Cmd.__init__(self)
        #self.prompt = "manager (agents)>"
        # Read and Set Configuration from Config File       
        self.ConfigFile = '../../app.conf'
        self.Config = ConfigParser.ConfigParser()
        self.Config.read(self.ConfigFile)
        self.DATABASE = self.ConfigSectionMap('COMMON')['database']    
        self.CON = sqlite3.connect(self.DATABASE)

   
    def ConfigSectionMap(self,section):
        dict1 = {}
        options = self.Config.options(section)
        for option in options:
            try:
                dict1[option] = self.Config.get(section, option)
                if dict1[option] == -1:
                    DebugPrint("skip: %s" % option)
            except:
                 print("exception on %s!" % option)
                 dict1[option] = None
        return dict1


    
    # Connect to DB
    def connect_db(self):
        return sqlite3.connect(self.DATABASE)
        
    def get_agents(self):
        with self.CON:
            sql = 'select * from agents'
            cur = self.CON.execute(sql)
            entries = cur.fetchall()
            #for item in entries:
            #    print item
        return entries
        
    def get_agent(self,agentid):
        with self.CON:
            sql = 'select * from agents where agentid=?'
            agentid = str(agentid)
            cur = self.CON.execute(sql,(agentid,))
            entries = cur.fetchall()
            #for item in entries:
            #    print item
        return entries
        
    def get_parameters(self,agentid):
        with self.CON:
            sql = 'select * from parameters where agentid=?'
            agentid = str(agentid)
            cur = self.CON.execute(sql,(agentid,))
            entries = cur.fetchall()
            #for item in entries:
            #    print item
        return entries        
        
    def get_agent_jobs(self,agentid):
        with self.CON:
            sql = 'select * from jobs where agentid=?'
            agentid = str(agentid)
            
            
            # print "****************"
            # print agentid
            # print "****************"
            
            cur = self.CON.execute(sql,(agentid,))
            entries = cur.fetchall()
            #for item in entries:
            #    print item
        return entries
        
    def add_job(self,agentid,jobtype,command):
        with self.CON:
            sql = "insert into jobs(agentid,status,jobtype,command,cmdresult,createtime,completedtime,pid) values (?,?,?,?,?,?,?,?)"
            cur = self.CON.execute(sql,(str(agentid),'new',str(jobtype),str(command),'',datetime.datetime.now(),'',''))
            self.CON.commit()
            #entries = cur.fetchall()
            #for item in entries:
            #    print item
        return
        
    def cancel_job(self,agentid,command):
        
        with self.CON:
            sql = 'select * from jobs where id=?'
            cur = self.CON.execute(sql,(str(command)))
            entries = cur.fetchall()
                        
            if str(entries[0][2]) == 'new':
                sql = "update jobs set status=?,completedtime=? where agentid=? and id=?"
                cur = self.CON.execute(sql,('canceled',datetime.datetime.now(),str(agentid),str(command)))
                self.CON.commit()            
            else:
                print "Only NEW jobs can be canceled"

        return
    
    def update_parameter(self, agentid, parameter, parametervalue):
        with self.CON:
            #sql = "insert into agents(agentid, hostname, osinfo, createtime, lastpolltime) values (?,?,?,?,?)"
            #sql = "select * from parameters where agentid = 'default' limit 1"
            #sql = "update jobs set status=?,completedtime=? where agentid=? and id=?"
            sql = "update parameters set " + parameter + "=? where agentid=?"
            cur = self.CON.execute(sql,(str(parametervalue), str(agentid)))
 
            self.CON.commit()       
        return
    
            
    def disconnect_db(self):
        if self.CON:
            self.CON.close()






    
    

        

   

    

