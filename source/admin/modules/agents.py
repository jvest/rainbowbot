''' View Module'''

import cmd, sys

# Non-standard imports
import modules


    
class Agents(cmd.Cmd):
    # Agents Options
  

    def __init__(self):
        cmd.Cmd.__init__(self)
        self.prompt = "rb:agents> "
        self.numbered_agentlist = []
        
    
    def do_back(self, line):
        
        return True
    
    def do_help(self, arg):
        output =  '\nAGENTS HELP   '+ '\n'
        output += '================' + '\n'
        output += "\t Command       \t Description" + "\n"
        output += "\t -------       \t -----------" + "\n"        
        output += "\t help          \t Help menu" + "\n"
        output += "\t back          \t Return to main" + "\n"
        output += "\t exit          \t Exit application" + "\n"
        output += "\t show [index]  \t Show all agents or specific agent" + "\n"
        output += "\t select <index>\t Set context to specific agent" + "\n"
        output += "" + "\n\n"
        
        print output
        
    def do_select(self, arg):
        '''Utilize the AGENT Module'''
       
        args = arg.split()
        
        if len(args) == 1: # Default
            try:
                index = int(args[0])
            except:
                print "Index must be integer"
                return
            if index < len(self.numbered_agentlist):
                s = modules.agent.Agent(self.numbered_agentlist[int(arg)])
                #s.myagentid = self.numbered_agentlist[int(arg)]
                s.cmdloop()   
            else:
                print "Not in range"
                return
        else:
            print "Invalid auguments "
            return
    

                        
    def do_show(self, arg):
    
        args = arg.split()
        
        if len(args) == 0: # Default
            self.showall()
        elif len(args) == 1: # Specific Index
            try:
                index = int(args[0])
            except:
                print "Index must be integer"
                return
            if index < len(self.numbered_agentlist):
                self.showindividual(index)
            else:
                print "Not in range"
                return
        else:
            print "Invalid auguments "
            return
            
    def showall(self):
            
        '''show current option values'''
        dbconnection = modules.dbconnect.DBconnect()
        dbconnection.connect_db()
        agentlist =  dbconnection.get_agents()
        dbconnection.disconnect_db()
        
        #Agent list
        output = ''
        count = 0
        self.numbered_agentlist[:] = []
        for item in agentlist:
            
            output += str(count) + "\t" + item[0] + "\t" + item[1] + "\t\t" + item[4] + "\n"  
            self.numbered_agentlist.append(item[0])      
            count += 1    

        #Header
        header = "\n"
        header += "Index\tAgentID \t\t\t\tHostname\tLast Poll Time" + "\n"
        header += "------------------------------------------------------------------------------------------\n"  
        
        #Footer
        footer =  "==========================================================================================\n"  
        footer += "COUNT: " + str(len(self.numbered_agentlist)) + "\n"
        
        
        
        print header + output + footer
  

    def showindividual(self, index):
            
        '''show current option values'''
        dbconnection = modules.dbconnect.DBconnect()
        dbconnection.connect_db()
        agentlist =  dbconnection.get_agents()
        index = int(index)
        #print "INDEX: " + str(index)
        #print "VALUE: " + self.numbered_agentlist[index]
        
        agentlist =  dbconnection.get_agent(self.numbered_agentlist[index])

        dbconnection.disconnect_db()
        
        #Agent list
        output = ''
        count = 0
        #self.numbered_agentlist[:] = []
        for item in agentlist:
            
            output += str(count) + "\t" + item[0] + "\t" + item[1] + "\t\t" + item[4] + "\n"  
            #self.numbered_agentlist.append(item[0])      
            count += 1         
        
        #Header
        header = "\n"
        header += "Index\tAgentID \t\t\t\tHostname\tLast Poll Time" + "\n"
        header += "------------------------------------------------------------------------------------------\n"  
        
        #Footer
        footer =  "==========================================================================================\n"  
        footer += "COUNT: " + str(len(self.numbered_agentlist)) + "\n"
        
        
        
        print header + output + footer
        
 
    def do_exit(self, arg):
        '''Exit the program'''
        sys.exit(1)
        
    

        

   

    

