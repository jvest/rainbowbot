# Rainbowbot Copyright (C) 2012 Joe Vest
# -------------------------------------------------
# Author:  Joe Vest
# Contact: mrjoevest at gmail.com
#   License details included with this application.
#   Referer to LICENSE.md for license details
# -------------------------------------------------
# 
# core.py

"""
Core module
"""
import cmd, sys

# Non-standard imports
import modules

ENDCOLOR = '\033[00m'
RED      = '\033[1;31m'
GREEN    = '\033[1;32m'
YELLOW   = '\033[1;33m'
BLUE     = '\033[1;34m'
MAGENTA  = '\033[1;35m'
CYAN     = '\033[1;36m'
WHITE    = '\033[00m'

class manager(cmd.Cmd):
    def __init__(self):
        cmd.Cmd.__init__(self)
        self.prompt = "rb> "
        
        ENDCOLOR = '\033[1;m'
        RED      = '\033[1;31m'
        GREEN    = '\033[1;32m'
        YELLOW   = '\033[1;33m'
        BLUE     = '\033[1;34m'
        MAGENTA  = '\033[1;35m'
        CYAN     = '\033[1;36m'
        
    def do_help(self, arg):
        '''Display Commands'''
        print ""
        output =  'HELP\n'
        output += '================' + '\n'
        output += "\t Command    \t Description" + "\n"
        output += "\t -------    \t -----------" + "\n"        
        output += "\t help       \t Help menu" + "\n"
        output += "\t banner     \t Display banner" + "\n"
        output += "\t exit     \t Exit application" + "\n"
        output += "\t Command  \t Description" + "\n"
        output += "\t -------  \t -----------" + "\n"      
        output += "\t agents \t Agents menu" + "\n"
        output += "\t defaults \t Application defaults menu" + "\n"
        output += "" + "\n"
        
        print output

    def do_banner(self, arg):
        '''Show Banner'''
        show_banner()
        show_about()
        
    def do_banner(self, arg):
        '''Show Banner'''
        
        show_banner()
        
        
    def do_agents(self, arg):
        '''Utilize the AGENTS Module'''
        s = modules.agents.Agents()
        s.cmdloop() 

    def do_defaults(self, arg):
        '''Utilize the DEFAULTS Module'''
        s = modules.defaults.Defaults()
        s.cmdloop()         

       
    def do_exit(self, arg):
        '''Exit the program'''
        sys.exit(1)
        
def show_about():
    """ Display about 
    """
    output =  ''
    output += '=================================================\n'
    output += "Rainbowbot - Captain K'Nuckles Release v0.9.4\n"
    output += "Project Creator:\tJoe Vest\n"
    output += "Contact:        \tmrjoevest(at)gmail.com\n"
    output += '================================================='
        
        
    print output

    
def show_banner():
    """ Display Banner
    """
    import random
        
    myint = random.randint(0, 2)
        
    if (myint == 0):
        output  =  RED     + '  _____       _       _                  ____        _   ' + ENDCOLOR + "\n"
        output +=  GREEN   + ' |  __ \     (_)     | |                |  _ \      | |  ' + ENDCOLOR + "\n"
        output +=  YELLOW  + ' | |__) |__ _ _ _ __ | |__   _____      |_|_) | ___ | |_ ' + ENDCOLOR + "\n"
        output +=  BLUE    + ' |  _  // _\ | | |_ \| |_ \ / _ \ \ /\ / / _ < / _ \| __|' + ENDCOLOR + "\n"
        output +=  MAGENTA + ' | | \ \ (_| | | | | | |_) | (_) \ V  V / |_) | (_) | |_ ' + ENDCOLOR + "\n"
        output +=  CYAN    + ' |_|  \_\__|_|_|_| |_|_.__/ \___/ \_/\_/|____/ \___/ \__|' + ENDCOLOR + "\n"
        print output
        
    elif (myint == 1):
        output =  '_________       __________    ' + RED     + '==========' + ENDCOLOR + "\n"
        output += '\______  \      \______   \   ' + GREEN   + '==========' + ENDCOLOR + "\n"
        output += '|       _/       |    |  _/   ' + YELLOW  + '==========' + ENDCOLOR + "\n" 
        output += '|    |   \       |    |   \   ' + BLUE    + '==========' + ENDCOLOR + "\n" 
        output += '|____|_  / ainbow|________/ot ' + MAGENTA + '==========' + ENDCOLOR + "\n"
        output += '       \/         \/          ' + CYAN    + '==========' + ENDCOLOR + "\n"
        print output
        
    elif (myint == 2):
        rainbowbot  = RED     + 'R' + ENDCOLOR
        rainbowbot += GREEN   + 'A' + ENDCOLOR 
        rainbowbot += YELLOW  + 'I' + ENDCOLOR
        rainbowbot += BLUE    + 'N' + ENDCOLOR
        rainbowbot += MAGENTA + 'B' + ENDCOLOR 
        rainbowbot += CYAN    + 'O' + ENDCOLOR 
        rainbowbot += RED     + 'W' + ENDCOLOR 
        rainbowbot += GREEN   + 'B' + ENDCOLOR 
        rainbowbot += YELLOW  + 'O' + ENDCOLOR 
        rainbowbot += BLUE    + 'T' + ENDCOLOR 
        
        output =  r'================================================================================= ' + "\n"
        output += r'     \	                                                                         ' + "\n"
        output += r'      \                                                                          ' + "\n"
        output += r'       \                                                                         ' + "\n"
        output += r'      (\\/)                                                                      ' + "\n"
        output += r'      (..  \         \         ___________________________                       ' + "\n"
        output += r'     _/  )  \______  /   _.----   ______'+rainbowbot+'_______   ----._               ' + "\n"
        output += r'    (oo / \        )/---   __.---   ___________________   ---.__   -.            ' + "\n"
        output += r'     ^^   (v  __( /  -----   __.---   _______________   ---.__   -.   -.         ' + "\n"
        output += r'          |||  |||                                    ---.__   -.   -.   .       ' + "\n"
        output += r'         //_| //_|                                             -.__ -.  -.   .   ' + "\n"
        output += r'=================================================================================' + "\n"
        print output
        
    show_about()
		
        
        
        
        
		
   
        


                        
