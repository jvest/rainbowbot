# Rainbowbot Copyright (C) 2012 Joe Vest
# -------------------------------------------------
# Author:  Joe Vest
# Contact: mrjoevest at gmail.com
#   License details included with this application.
#   Referer to LICENSE.md for license details
# -------------------------------------------------
# 
# admin.py

import os, sys, cmd
import ConfigParser

# Non-standard imports
from core.core import *

# Read and Set Configuration from Config File
Config = ConfigParser.ConfigParser()
Config.read('../../app.conf')

def ConfigSectionMap(section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

EULAACCEPTED = ConfigSectionMap('EULA')['eulaaccepted']

# Check if EULA was accepted

if EULAACCEPTED != 'YES':
    print 'Rainbowbot Copyright (C) 2012 Joe Vest'
    print 'Author:  Joe Vest'
    print 'Contact: mrjoevest at gmail.com'
    print 'License details included with this application.'
    print 'Referer to LICENSE.md for license details'
    print '-------------------------------------------------'
    print "EULA must be read and accepted before use"
    print "EULA can be found in LICENSE.md"
    print "To accept EULA, update the application configuration (app.conf)"
    print '-------------------------------------------------'
    print ''

    sys.exit()



show_banner()

# Check if running as root
#if os.getuid() != 0:
#    print "[-] Error:  You must run with root priviledges!"
#    sys.exit(1)



if __name__ == "__main__":
   manager().cmdloop()
