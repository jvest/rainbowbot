#!/bin/bash

echo '======================================================================='
echo '     \                                                                 '
echo '      \                                                                '
echo '       \                                                               '
echo '      (\\/)                                                            '
echo '      (..  \         \         ___________________________             '
echo '     _/  )  \______  /   _.----   ______rainbowbot_______ ----._       '   
echo '    (oo / \        )/---   __.---   ___________________   ---.__   -.  '
echo '     ^^   (v  __( /  -----   __.---   _______________   ---.__   -.   -'
echo '          |||  |||                                    ---.__   -.   -. '
echo '         //_| //_|                                             -.__ -. '
echo '======================================================================='

echo 'Rainbowbot Copyright (C) 2012 Joe Vest'
echo 'INSTALLER'
echo ''
echo '================================================='
echo '[!] Some commands require elevated priviledges'
echo '[!] You may be asked to provide Sudo credentials.'
echo '================================================='
echo [*] Would you like to install and setup Rainbowbot?
echo [*] Wine will be configured as user: `whoami` 
echo [?] Begin Install? y/N
read x
if [ "$x" = "y" ]
then
	
	# Install packages


    if [ `uname -m` == 'x86_64' ]
	then
		echo
		echo ' [*] Adding i386 Architecture To x86_64 System'
		sudo dpkg --add-architecture i386
		echo
		echo ' [*] Updating Apt Package Lists'
		sudo apt-get update
		echo
		echo ' [*] Installing Wine i386 Binaries'
		sudo apt-get -y install wine-bin:i386 ia32-libs
	fi

	echo [*] Must run apt-get as root
	sudo apt-get install -y wine winbind python-crypto python-dev python-pip build-essential libssl-dev

	echo '[*] Installing python modules'
	pip install Flask cherrypy pyOpenSSL requests

	# Download required files
	TEMP_DIR=/tmp/wine_config
	mkdir -p $TEMP_DIR

	REQUIRED_FILES="https://bitbucket.org/jvest/wine-python-2.7/get/master.zip"

	wget -O $TEMP_DIR/master.zip $REQUIRED_FILES --no-check-certificate

	unzip -u -d $TEMP_DIR/ $TEMP_DIR/master.zip
	mv -n -v $TEMP_DIR/jvest*/* $TEMP_DIR/
	rm -rf $TEMP_DIR/jvest*/

	# Configure Wine
 	echo ' [*] Preparing Wine Directories'
  	mkdir -p ~/.wine/drive_c/Python27/Lib/
  	cp $TEMP_DIR/distutils -u -r ~/.wine/drive_c/Python27/Lib/
  	cp $TEMP_DIR/tcl -u -r ~/.wine/drive_c/Python27/
  	cp $TEMP_DIR/Tools -u -r ~/.wine/drive_c/Python27/	

  	############################################
  	# Interactive Install
  	############################################
	# Install and Configure Python in Wine
	echo ' [*] Installing Python in Wine'
	wine msiexec /i $TEMP_DIR/python-2.7.5.msi
	wine $TEMP_DIR/pywin32-218.win32-py2.7.exe
	wine $TEMP_DIR/WMI-1.4.9.win32.exe
	wine $TEMP_DIR/setuptools-0.6c11.win32-py2.7.exe
	wine c:/Python27/Scripts/easy_install requests
	wine c:/Python27/Scripts/easy_install argparse
	unzip -u $TEMP_DIR/pyinstaller-2.0.zip -d ~/.wine/drive_c/Python27/

	############################################
	# AUTOMATED INSTALL - Causing failures
	############################################
	# # Install Setup Files
	# wine msiexec /i $TEMP_DIR/python-2.7.5.msi /qn

	# #wine pywin32-218.win32-py2.7.exe
	# PYWIN_DIR=$TEMP_DIR/pywin32
	# mkdir -p $PYWIN_DIR
	# unzip -u -d $PYWIN_DIR $TEMP_DIR/pywin32-218.win32-py2.7.exe
	# mv -u -n $PYWIN_DIR/PLATLIB/* ~/.wine/drive_c/Python27/Lib/site-packages/
	# mv -u -n $PYWIN_DIR/SCRIPTS/* ~/.wine/drive_c/Python27/Scripts/
	# wine c:/Python27/python.exe c:/Python27/Scripts/pywin32_postinstall.py -install

	# #wine pycrypto-2.6.win32-py2.7.exe
	# PYCRYPT_DIR=$TEMP_DIR/pycrypto
	# unzip -u -d $PYCRYPT_DIR $TEMP_DIR/pycrypto-2.6.win32-py2.7.exe
	# mv -n -v $PYCRYPT_DIR/PLATLIB/* ~/.wine/drive_c/Python27/Lib/site-packages/

	# #wine WMI-1.49.win32.exe
	# WMI_DIR=$TEMP_DIR/wmi
	# unzip -u -d $WMI_DIR $TEMP_DIR/WMI-1.4.9.win32.exe
	# mv -u -n $WMI_DIR/PLATLIB/* ~/.wine/drive_c/Python27/Lib/site-packages/
	# mv -u -n $WMI_DIR/PURELIB/* ~/.wine/drive_c/Python27/Lib/site-packages/
	# mv -u -n $WIN_DIR/SCRIPTS/* ~/.wine/drive_c/Python27/Scripts/

	# #wine setuptools
	# SETUPTOOLS_DIR=$TEMP_DIR/setuptools
	# unzip -u -d $SETUPTOOLS_DIR $TEMP_DIR/setuptools-0.6c11.win32-py2.7.exe
	# mv -u -n $SETUPTOOLS_DIR/PLATLIB/* ~/.wine/drive_c/Python27/Lib/site-packages/
	# mv -u -n $SETUPTOOLS_DIR/PURELIB/* ~/.wine/drive_c/Python27/Lib/site-packages/
	# mv -u -n $SETUPTOOLS_DIR/SCRIPTS/* ~/.wine/drive_c/Python27/Scripts/

	############################################

	#pyinstaller-2.0
	unzip -u $TEMP_DIR/pyinstaller-2.0.zip -d ~/.wine/drive_c/Python27/

	# Fix for i386 dependencies when running pyinstaller under wine
	# Error Observed:
	# p11-kit: couldn't load module: /usr/lib/i386-linux-gnu/pkcs11/gnome-keyring-pkcs11.so: /usr/lib/i386-linux-gnu/pkcs11/gnome-keyring-pkcs11.so: cannot open shared object file: No such file or directory
	# Reference: http://askubuntu.com/questions/127848/wine-cant-find-gnome-keyring-pkcs11-so

	echo '[*] Fixing keyring error issue'
	mkdir -p $TEMP_DIR/gnome-keyring
	apt-get download gnome-keyring:i386  ## download the i386 version of gnome-keyring
	mv ./gnome-keyring* $TEMP_DIR/gnome-keyring
	ar x $TEMP_DIR/gnome-keyring/gnome-keyring*.deb
	rm -f ./debian-binary
	rm -f ./control.tar.*
	mv ./data.tar.* $TEMP_DIR/gnome-keyring
	tar xf $TEMP_DIR/gnome-keyring/data.tar.* --directory=$TEMP_DIR/gnome-keyring
	sudo mv $TEMP_DIR/gnome-keyring/usr/lib/i386-linux-gnu/pkcs11 /usr/lib/i386-linux-gnu/


	echo '[*] Install Complete'

fi