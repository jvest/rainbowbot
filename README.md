# Rainbowbot
#### Command and Control Botnet over HTTP(s)

```
================================================================================= 
     \                                                                           
      \                                                                          
       \                                                                         
      (\\/)                                                                      
      (..  \         \         ___________________________                       
     _/  )  \______  /   _.----   ______rainbowbot_______   ----._               
    (oo / \        )/---   __.---   ___________________   ---.__   -.            
     ^^   (v  __( /  -----   __.---   _______________   ---.__   -.   -.         
          |||  |||                                    ---.__   -.   -.   .       
         //_| //_|                                             -.__ -.  -.   .   
=================================================================================
```
Rainbowbot Copyright (C) 2012 Joe Vest

---

## Features
---

* Asyncronous communication from agent to C&C
* OS command execution
* Upload/Download files
* Direct shellcode injection
* Shellcode injection via Syringe
* Adjustable agent options configured through common configuration file (app.conf)
  
  httpcclist
  : List of C&C servers
  : 192.168.1.78:443,127.0.0.1:443

  initialwaittime
  : Initial time to wait before agent makes first contact with C&C
  : 30
  
  polltime
  : Time in seconds to poll C&C for new jobs
  : 50

  polldrift
  : Time in seconds added or subtracted to polltime as each poll.  Example:  Polltime of 30 seconds with a polldrift of 5 seconds would result in an actual polltime between 25 - 35 seconds.
  : 5

  useragentstring
  : User agent to use with HTTP communications
  : Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 1.1.4322; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648)

  allowedtimes
  : Times agent will attempt to contact C&C
  : 0001,2359

  killdate
  : Date agent will cease to function
  : 20130910

  protocol
  : Protocol to use for agent - C&C communications
  : HTTPS

## Components
---

* Server
* Admin Console
* Remote Agent

## Quick Start
---
1. Reset database (New instances only)
    > ./reset_database.sh
2. Modify Config file (app.conf)
3. Start Server
    > sudo ./start_server.sh
4. Start Admin Console
    > ./start_admin.sh
5. Build agent
    > python ./build_agent.py
6. Deploy and run agent.exe on target(s)
7. Manage agent(s) using Admin Console

## Installation
---
## Tested on

 * Ubuntu 12.04 Linux 
 * Kali Linux

The install script will setup Kali linux or Ubuntu 12.04 for both 32 and 64 bit versions

In Ubuntu, you will be asked to provide creds to issue commands with Sudo

run setup.sh to install
   
## Application Structure

### Configuration    

#### app.conf

    > Main configration parameters

#### ./source/agent/constants.py

    > Starting configuration for agents.  Values set here are used at agent compile time.  Value can be reset dynamiclly at run time through the admin console.  If an is restarted, the default values are used again and will be reset at agent initialization.

##### Communication Protocol and URL Structure
 |Function	|	URL|
 ----------------
 |Main         |/                    |
 |JSON Parser  |/ReceiveJSON/        |
 |Reporting    |/reports             |
 |File Download|/downloads/<filename>|

### JSON Format
#### Request from Client

> {"jobtype":<jobtype_integer>,"jobpayload":{<payload data 1>,<payload data 2>,<payload data X>}}

#### Response from Server
> {"result":true/false,"resultpayload":{<payload data 1>,<payload data 2>,<payload data X>}}

### SERVER JOBTYPES

* json_data['jobtype'] == 1: # Alive Test
* json_data['jobtype'] == 2: # Register Agent
* json_data['jobtype'] == 3: # Initialize Agent
* json_data['jobtype'] == 4: # Get Jobs
* json_data['jobtype'] == 5: # Update Job Status to Queued
* json_data['jobtype'] == 6: # Update Job Status to Running
* json_data['jobtype'] == 7: # Update Job Status to Complete
* json_data['jobtype'] == 8: # Upload to agent
* json_data['jobtype'] == 9: # Download from agent

#### CLIENT JOBTYPES
* job["jobtype"] == 'command'
* job["jobtype"] == 'shellcode'
* job["jobtype"] == 'syringe'
* job["jobtype"] == 'downloadfromagent'
* job["jobtype"] == 'uploadtoagent'



